#include "TtbarDNN/DnnPreparator.h"

#include <iostream>

int main(int argc, char *argv[]) {

    if (argc != 2) {
        std::cerr << "One parameter is expected but " << argc -1 << " parameters provided" << std::endl;
        exit(EXIT_FAILURE);
    }

    const std::string name = argv[1];

    DnnPreparator preparator(name);

    preparator.ReadConfig();

    preparator.ProcessFiles();

    /// successful run
    return 0;
}
