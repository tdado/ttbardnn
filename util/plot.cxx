#include "TtbarDNN/Plotter.h"

#include "TError.h"

#include <iostream>
#include <string>
#include <vector>

int main (int argc, char *argv[]) {
    if (argc != 2 && argc != 3) {
        std::cerr << "Expecting one or two arguments but " << argc-1 << " arguments provided" << std::endl;
        exit(EXIT_FAILURE);  
    }

    const std::string folder = argv[1];
    bool use_syst(false);
    std::string syst_param = "";

    if (argc == 3) {
        syst_param = argv[2];
        if (syst_param == "syst") {
            use_syst = true;
        } else {
            std::cerr << "Trith argument can only be 'syst'" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    gErrorIgnoreLevel = kError;

    Plotter plotter(folder);

    static const std::vector<std::string> ttbar_names = {"ttbar_PP8_FS",
                                                         "ttbar_PH7_0_4_AFII",
                                                         "ttbar_aMCNLO_Herwig7_1_3_AFII",
                                                         "ttbar_PH7_1_3_AFII",
                                                         "ttbar_PP8_hdamp_AFII",
                                                         "ttbar_aMCNLO_Pythia8_AFII"};
    
    static const std::vector<std::string> bkg_names = {"SingleTop_PP8_s_chan_FS",
                                                       "SingleTop_PP8_t_chan_FS",
                                                       "SingleTop_PP8_tW_chan_FS",
                                                       "Wjets_Sherpa_FS",
                                                       "Zjets_Sherpa_FS",
                                                       "Diboson_Sherpa_FS",
                                                       "ttV_aMCNLO_Pythia8_FS",
                                                       "ttH_PP8_FS",
                                                       "Multijet"};

    static const std::vector<std::string> special_names = {"ttbar_PP8_AFII",
                                                           "SingleTop_PP8_s_chan_AFII",
                                                           "SingleTop_PP8_t_chan_AFII",
                                                           "SingleTop_PP8_tW_chan_AFII",
                                                           "ttbar_PP8_hdamp_AFII",
                                                           "ttbar_PP8_shower_decor_AFII",
                                                           "SingleTop_DS_PP8_tW_chan_FS",
                                                           "SingleTop_PH7_0_4_s_chan_AFII",
                                                           "SingleTop_PH7_0_4_t_chan_AFII",
                                                           "SingleTop_PH7_0_4_tW_chan_AFII",
                                                           "SingleTop_aMCNLO_Pythia8_s_chan_AFII",
                                                           "SingleTop_aMCNLO_Pythia8_t_chan_AFII",
                                                           "SingleTop_aMCNLO_Pythia8_tW_chan_AFII"
                                                           };

    plotter.SetTTbarNames(ttbar_names);
    plotter.SetBackgroundNames(bkg_names);
    plotter.SetSpecialNames(special_names);
    plotter.SetDataName("Data");
    plotter.SetAtlasLabel("Internal");
    plotter.SetLumiLabel("139");
    plotter.SetCollection("pflow");
    plotter.SetNominalTTbarName("ttbar_PP8_FS");
    plotter.SetSystShapeOnly(false);
    plotter.SetRunSyst(use_syst);
    
    plotter.OpenRootFiles();

    /// Set all systematics
    SystHistoManager syst_manager{};
    
    std::vector<std::string> all(bkg_names);
    all.emplace_back(plotter.GetNominalTTbarName());

    /// Set tree systematics
    for (const auto& ifile : all) {
        if (ifile == "Multijet") continue;
        for (int ib = 0; ib < 9; ++ib) {
            syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_B_"+std::to_string(ib)+"_up", "BTAG_B_"+std::to_string(ib)+"_down", "", "nominal", ifile);
        }
        for (int ic = 0; ic < 4; ++ic) {
            syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_C_"+std::to_string(ic)+"_up", "BTAG_C_"+std::to_string(ic)+"_down", "", "nominal", ifile);
        }
        for (int il = 0; il < 4; ++il) {
            syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_Light_"+std::to_string(il)+"_up", "BTAG_Light_"+std::to_string(il)+"_down", "", "nominal", ifile);
        }
        syst_manager.AddTwoSidedSyst(ifile, "", "el_trigger_up", "el_trigger_down", "", "nominal", ifile);
        syst_manager.AddTwoSidedSyst(ifile, "", "el_reco_up", "el_reco_down", "", "nominal", ifile);
        syst_manager.AddTwoSidedSyst(ifile, "", "el_id_up", "el_id_down", "", "nominal", ifile);
        syst_manager.AddTwoSidedSyst(ifile, "", "el_isol_up", "el_isol_down", "", "nominal", ifile);
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_trigger_stat_up", "mu_trigger_stat_down", "", "nominal", ifile);
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_trigger_syst_up", "mu_trigger_syst_down", "", "nominal", ifile);
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_stat_up", "mu_id_stat_down", "", "nominal", ifile);
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_syst_up", "mu_id_syst_down", "", "nominal", ifile);
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_stat_lowpt_up", "mu_id_stat_lowpt_down", "", "nominal", ifile);
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_syst_lowpt_up", "mu_id_syst_lowpt_down", "", "nominal", ifile);
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_isol_stat_up", "mu_isol_stat_down", "", "nominal", ifile);
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_isol_syst_up", "mu_isol_syst_down", "", "nominal", ifile);
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_ttva_stat_up", "mu_ttva_stat_down", "", "nominal", ifile);
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_ttva_syst_up", "mu_ttva_syst_down", "", "nominal", ifile);
        syst_manager.AddTwoSidedSyst(ifile, "", "jvt_up", "jvt_down", "", "nominal", ifile);
        syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_extrapolation_from_charm_up", "BTAG_extrapolation_from_charm_down", "", "nominal", ifile);
        syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_extrapolation_up", "BTAG_extrapolation_down", "", "nominal", ifile);
        syst_manager.AddTwoSidedSyst(ifile, "", "pileup_up", "pileup_down", "", "nominal", ifile);

        /// Luminosity
        syst_manager.AddNormSyst(ifile, 0.017, 0.017);
        
        /// MC stat
        syst_manager.AddMCstatSyst(ifile);
    }

    /// Add individual systematics
    syst_manager.AddOneSidedSyst("ttbar_PH7_1_3_AFII","nominal","ttbar_aMCNLO_Herwig7_1_3_AFII", "nominal", "ttbar_PP8_AFII");
    syst_manager.AddOneSidedSyst("ttbar_PH7_1_3_AFII","nominal","ttbar_PP8_AFII", "nominal", "ttbar_PP8_AFII");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_AFII", "", "muR10muF20", "muR10muF05", "", "nominal", "ttbar_PP8_AFII");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_AFII", "", "muR20muF10", "muR05muF10", "", "nominal", "ttbar_PP8_AFII");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_AFII", "", "Var3cUp", "Var3cDown", "", "nominal", "ttbar_PP8_AFII");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_AFII", "", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "ttbar_PP8_AFII");

    syst_manager.AddNormSyst("ttbar_PP8_AFII", 0.05, 0.05);

    /// Pass all systematics
    plotter.SetAllSysts(syst_manager.GetAllSystematics());
    
    /// 1D Data/MC plots
    plotter.PlotDataMCPlots("jet1_pt","Leading jet p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("jet1_pt","Leading jet p_{T} [GeV]", "Events", "GeV", true);
    
    /// Make html table with yields
    plotter.CalculateYields();

    plotter.CloseRootFiles();

    /// Successful run
    return 0;
}
