#ifndef COMMON_H_
#define COMMON_H_

#include <string>
#include <vector>

class TH1D;

namespace Common {
    enum LEPTONTYPE {
        EL = 0,
        MU = 1,
    };

    void AddOverflow(TH1D* hist);

    std::string RemoveSubstrings(const std::string& str, const std::vector<std::string>& remove);
    
    float GetMax(TH1D* h1, TH1D* h2);
}

#endif
