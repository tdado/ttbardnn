#ifndef HISTOMAKER_H_
#define HISTOMAKER_H_

#include "TtbarDNN/ControlPlots.h"

#include "TtbarDNN/Common.h"

#include <memory>
#include <string>
#include <vector>

class WeightManager;

class HistoMaker {

public:
    explicit HistoMaker(const std::string& type,
                        const std::string& systematics,
                        const std::vector<std::string>& sfsyst_list);

    void SetCurrentSyst(const std::string& syst){m_current_syst = syst;}
    void SetIsMC(const bool& isMC){m_isMC = isMC;}
    
    void FillHistos(const WeightManager& wei_manager,
                    const std::string& tree_name,
                    const std::string& path);
    void Finalise();
    void WriteHistosToFile(const std::string& path, const std::string& name);

private:

    std::string m_type;
    std::string m_systematics;
    std::string m_current_syst;
    bool m_isMC;
    bool m_isTT;
    bool m_isPP8;
    bool m_isNominal;
    std::string m_tree_name;
    std::vector<std::string> m_sfsyst_list;
    Common::LEPTONTYPE m_lepton_type;

    std::unique_ptr<ControlPlots> m_control_plots;
};

#endif
