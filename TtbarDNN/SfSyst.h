#ifndef SFSYST_H_
#define SFSYST_H_

#include <string>
#include <vector>

class Event;

enum SFSYSTENUM {
   PILEUP_UP,
   PILEUP_DOWN,
   EL_TRIGGER_UP,
   EL_TRIGGER_DOWN,
   EL_RECO_UP,
   EL_RECO_DOWN,
   EL_ID_UP,
   EL_ID_DOWN,
   EL_ISOL_UP,
   EL_ISOL_DOWN,
   MU_TRIGGER_STAT_UP,
   MU_TRIGGER_STAT_DOWN,
   MU_TRIGGER_SYST_UP,
   MU_TRIGGER_SYST_DOWN,
   MU_ID_STAT_UP,
   MU_ID_STAT_DOWN,
   MU_ID_SYST_UP,
   MU_ID_SYST_DOWN,
   MU_ID_STAT_LOWPT_UP,
   MU_ID_STAT_LOWPT_DOWN,
   MU_ID_SYST_LOWPT_UP,
   MU_ID_SYST_LOWPT_DOWN,
   MU_ISOL_STAT_UP,
   MU_ISOL_STAT_DOWN,
   MU_ISOL_SYST_UP,
   MU_ISOL_SYST_DOWN,
   MU_TTVA_STAT_UP,
   MU_TTVA_STAT_DOWN,
   MU_TTVA_SYST_UP,
   MU_TTVA_SYST_DOWN,
   JVT_UP,
   JVT_DOWN,
   BTAG_B_0_UP,
   BTAG_B_0_DOWN,
   BTAG_B_1_UP,
   BTAG_B_1_DOWN,
   BTAG_B_2_UP,
   BTAG_B_2_DOWN,
   BTAG_B_3_UP,
   BTAG_B_3_DOWN,
   BTAG_B_4_UP,
   BTAG_B_4_DOWN,
   BTAG_B_5_UP,
   BTAG_B_5_DOWN,
   BTAG_B_6_UP,
   BTAG_B_6_DOWN,
   BTAG_B_7_UP,
   BTAG_B_7_DOWN,
   BTAG_B_8_UP,
   BTAG_B_8_DOWN,
   BTAG_C_0_UP,
   BTAG_C_0_DOWN,
   BTAG_C_1_UP,
   BTAG_C_1_DOWN,
   BTAG_C_2_UP,
   BTAG_C_2_DOWN,
   BTAG_C_3_UP,
   BTAG_C_3_DOWN,
   BTAG_C_4_UP,
   BTAG_C_4_DOWN,
   BTAG_LIGHT_0_UP,
   BTAG_LIGHT_0_DOWN,
   BTAG_LIGHT_1_UP,
   BTAG_LIGHT_1_DOWN,
   BTAG_LIGHT_2_UP,
   BTAG_LIGHT_2_DOWN,
   BTAG_LIGHT_3_UP,
   BTAG_LIGHT_3_DOWN,
   BTAG_LIGHT_4_UP,
   BTAG_LIGHT_4_DOWN,
   BTAG_LIGHT_5_UP,
   BTAG_LIGHT_5_DOWN,
};

namespace SfSyst {

    std::vector<SFSYSTENUM> GetAllSFs();

    std::string EnumToString(const SFSYSTENUM& value);
    
    float EnumToWeight(const SFSYSTENUM& value, const Event& event);
}

#endif
