#ifndef DNNPREPARATOR_H_
#define DNNPREPARATOR_H_

#include <string>
#include <vector>

class DnnPreparator {

public:

    explicit DnnPreparator(const std::string& name);

    ~DnnPreparator() = default;

    void ReadConfig();

    void ProcessFiles();

private:

    std::string m_name;
    std::vector<std::string> m_file_path;
    std::vector<std::string> m_campaign;

    void ReadFileList(const std::string& path);
};

#endif
