#ifndef DNNTREE_H_
#define DNNTREE_H_

#include "TFile.h"
#include "TRandom3.h"
#include "TTree.h"

#include <string>
#include <memory>

class Event;

class DnnTree {
public:
    explicit DnnTree(const std::string& name);

    void ProcessSingleEvent(const Event& event, const float& weight);

    void Write();
private:

    std::string m_name;
    std::unique_ptr<TFile> m_file;
    std::vector<TTree*> m_trees;
    std::unique_ptr<TRandom3> m_random;

    float m_weight;
    float m_jet1_pt;
    float m_jet1_eta;
    float m_jet1_phi;
    float m_jet1_e;
    int   m_jet1_tag_bin;
    float m_jet2_pt;
    float m_jet2_eta;
    float m_jet2_phi;
    float m_jet2_e;
    int   m_jet2_tag_bin;
    float m_jet3_pt;
    float m_jet3_eta;
    float m_jet3_phi;
    float m_jet3_e;
    int   m_jet3_tag_bin;
    float m_jet4_pt;
    float m_jet4_eta;
    float m_jet4_phi;
    float m_jet4_e;
    int   m_jet4_tag_bin;
    int   m_jet_n;
    float m_had_top_pt;
    float m_had_top_eta;
    float m_had_top_phi;
    float m_had_top_m;
    float m_had_W_pt;
    float m_had_W_eta;
    float m_had_W_phi;
    float m_had_W_m;
    float m_W_jets_dR;
    float m_b_lep_dR;
    float m_mlb;
    float m_pt_lb;

    std::size_t GetNumberOfMatched(const std::vector<int>& indices, const Event& event) const;
};

#endif
