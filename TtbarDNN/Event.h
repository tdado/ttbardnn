//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Sep 19 12:11:07 2019 by ROOT version 6.10/08
// from TTree nominal/tree
// found on file: ../test/input_files/ttbar_pflow.root
//////////////////////////////////////////////////////////

#ifndef EVENT_H_
#define EVENT_H_

#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>

class Event {
public :
   bool m_isMC;
   bool m_isTT;
   bool m_isLoose;
   bool m_isSyst;
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   std::vector<float>   *mc_generator_weights;
   Float_t         weight_mc;
   Float_t         weight_pileup;
   Float_t         weight_leptonSF;
   Float_t         weight_bTagSF_DL1r_Continuous;
   Float_t         weight_jvt;
   Float_t         weight_pileup_UP;
   Float_t         weight_pileup_DOWN;
   Float_t         weight_leptonSF_EL_SF_Trigger_UP;
   Float_t         weight_leptonSF_EL_SF_Trigger_DOWN;
   Float_t         weight_leptonSF_EL_SF_Reco_UP;
   Float_t         weight_leptonSF_EL_SF_Reco_DOWN;
   Float_t         weight_leptonSF_EL_SF_ID_UP;
   Float_t         weight_leptonSF_EL_SF_ID_DOWN;
   Float_t         weight_leptonSF_EL_SF_Isol_UP;
   Float_t         weight_leptonSF_EL_SF_Isol_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
   Float_t         weight_jvt_UP;
   Float_t         weight_jvt_DOWN;
   std::vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_B_up;
   std::vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_C_up;
   std::vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_Light_up;
   std::vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_B_down;
   std::vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_C_down;
   std::vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_Light_down;
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   Float_t         mu_actual;
   UInt_t          backgroundFlags;
   std::vector<float>   *el_pt;
   std::vector<float>   *el_eta;
   std::vector<float>   *el_cl_eta;
   std::vector<float>   *el_phi;
   std::vector<float>   *el_e;
   std::vector<float>   *el_charge;
   std::vector<char>    *el_CF;
   std::vector<char>    *el_isTight;
   std::vector<char>    *mu_isTight;
   std::vector<float>   *mu_pt;
   std::vector<float>   *mu_eta;
   std::vector<float>   *mu_phi;
   std::vector<float>   *mu_e;
   std::vector<float>   *mu_charge;
   std::vector<float>   *jet_pt;
   std::vector<float>   *jet_eta;
   std::vector<float>   *jet_phi;
   std::vector<float>   *jet_e;
   std::vector<float>   *jet_jvt;
   std::vector<int>     *jet_tagWeightBin_DL1r_Continuous;
   Float_t         met_met;
   Float_t         met_phi;
   Int_t           ejets_2015;
   Int_t           ejets_2016;
   Int_t           ejets_2017;
   Int_t           ejets_2018;
   Int_t           mujets_2015;
   Int_t           mujets_2016;
   Int_t           mujets_2017;
   Int_t           mujets_2018;
   Char_t          is_AFII;
   Char_t          isDilepton;
   Int_t           index_W_up; 
   Int_t           index_W_down; 
   Int_t           index_b_lep; 
   Int_t           index_b_had; 
   Float_t         W_quark_up_pt;
   Float_t         W_quark_up_eta;
   Float_t         W_quark_up_phi;
   Float_t         W_quark_up_m;
   Float_t         W_quark_down_pt;
   Float_t         W_quark_down_eta;
   Float_t         W_quark_down_phi;
   Float_t         W_quark_down_m;
   Float_t         b_quark_lep_pt;
   Float_t         b_quark_lep_eta;
   Float_t         b_quark_lep_phi;
   Float_t         b_quark_lep_m;
   Float_t         b_quark_had_pt;
   Float_t         b_quark_had_eta;
   Float_t         b_quark_had_phi;
   Float_t         b_quark_had_m;
   Float_t         lepton_pt;
   Float_t         lepton_eta;
   Float_t         lepton_phi;
   Float_t         lepton_m;
   Float_t         neutrino_pt;
   Float_t         neutrino_eta;
   Float_t         neutrino_phi;
   Float_t         neutrino_m;

   // List of branches
   TBranch        *b_mc_generator_weights;   //!
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_leptonSF;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous;   //!
   TBranch        *b_weight_jvt;   //!
   TBranch        *b_weight_pileup_UP;   //!
   TBranch        *b_weight_pileup_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_jvt_UP;   //!
   TBranch        *b_weight_jvt_DOWN;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_extrapolation_up;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_mu_actual;   //!
   TBranch        *b_backgroundFlags;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_CF;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_el_isTight;   //!
   TBranch        *b_mu_isTight;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_jet_tagWeightBin_DL1r_Continuous;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_ejets_2015;   //!
   TBranch        *b_ejets_2016;   //!
   TBranch        *b_ejets_2017;   //!
   TBranch        *b_ejets_2018;   //!
   TBranch        *b_mujets_2015;   //!
   TBranch        *b_mujets_2016;   //!
   TBranch        *b_mujets_2017;   //!
   TBranch        *b_mujets_2018;   //!
   TBranch        *b_is_AFII;   //!
   TBranch        *b_index_W_up;   //!
   TBranch        *b_index_W_down;   //!
   TBranch        *b_index_b_lep;   //!
   TBranch        *b_index_b_had;   //!
   TBranch        *b_isDilepton;   //!
   TBranch        *b_W_quark_up_pt;   //!
   TBranch        *b_W_quark_up_eta;   //!
   TBranch        *b_W_quark_up_phi;   //!
   TBranch        *b_W_quark_up_m;   //!
   TBranch        *b_W_quark_down_pt;   //!
   TBranch        *b_W_quark_down_eta;   //!
   TBranch        *b_W_quark_down_phi;   //!
   TBranch        *b_W_quark_down_m;   //!
   TBranch        *b_b_quark_lep_pt;   //!
   TBranch        *b_b_quark_lep_eta;   //!
   TBranch        *b_b_quark_lep_phi;   //!
   TBranch        *b_b_quark_lep_m;   //!
   TBranch        *b_b_quark_had_pt;   //!
   TBranch        *b_b_quark_had_eta;   //!
   TBranch        *b_b_quark_had_phi;   //!
   TBranch        *b_b_quark_had_m;   //!
   TBranch        *b_lepton_pt;   //!
   TBranch        *b_lepton_eta;   //!
   TBranch        *b_lepton_phi;   //!
   TBranch        *b_lepton_m;   //!
   TBranch        *b_neutrino_pt;   //!
   TBranch        *b_neutrino_eta;   //!
   TBranch        *b_neutrino_phi;   //!
   TBranch        *b_neutrino_m;   //!

   Event(TTree *tree, bool isMC, bool isTT, bool isLoose, bool isSyst);
   virtual ~Event();
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
};

#endif
