#ifndef CONTROLPLOTS_H_
#define CONTROLPLOTS_H_

#include "TH1D.h"

#include <string>
#include <vector>

class Event;
class TFile;

class ControlPlots {

public:
    ControlPlots();

    ~ControlPlots() = default;

    void Init(const std::vector<std::string>& sf_syst_names);

    void FillAllHistos(const Event& event,
                       const int& index1,
                       const int& index2,
                       const bool& isMC,
                       const std::vector<double>& weights);
    
    void Finalise();

    void Write(const std::string& name, TFile* file);

private:

    std::vector<std::string> m_sf_syst_names;
    std::vector<double> m_weights;
    bool m_isMC;

    std::vector<TH1D> m_jet1_pt;
    std::vector<TH1D> m_jet1_eta;
    std::vector<TH1D> m_jet1_phi;
    std::vector<TH1D> m_jet2_pt;
    std::vector<TH1D> m_jet2_eta;
    std::vector<TH1D> m_jet2_phi;
    std::vector<TH1D> m_jet3_pt;
    std::vector<TH1D> m_jet3_eta;
    std::vector<TH1D> m_jet3_phi;
    std::vector<TH1D> m_jet4_pt;
    std::vector<TH1D> m_jet4_eta;
    std::vector<TH1D> m_jet4_phi;
    std::vector<TH1D> m_jet5_pt;
    std::vector<TH1D> m_jet5_eta;
    std::vector<TH1D> m_jet5_phi;
    std::vector<TH1D> m_jet6_pt;
    std::vector<TH1D> m_jet6_eta;
    std::vector<TH1D> m_jet6_phi;
    std::vector<TH1D> m_bjet1_pt;
    std::vector<TH1D> m_bjet1_eta;
    std::vector<TH1D> m_bjet1_phi;
    std::vector<TH1D> m_bjet2_pt;
    std::vector<TH1D> m_bjet2_eta;
    std::vector<TH1D> m_bjet2_phi;
    std::vector<TH1D> m_HT;
    std::vector<TH1D> m_jet_n;
    std::vector<TH1D> m_btag_bin;

    std::vector<TH1D> m_el_pt;
    std::vector<TH1D> m_el_eta;
    std::vector<TH1D> m_el_phi;
    std::vector<TH1D> m_mu_pt;
    std::vector<TH1D> m_mu_eta;
    std::vector<TH1D> m_mu_phi;
    std::vector<TH1D> m_met;
    std::vector<TH1D> m_met_phi;
    std::vector<TH1D> m_average_mu;
    std::vector<TH1D> m_actual_mu;
    
    std::vector<TH1D> m_Wjet1_pt;
    std::vector<TH1D> m_Wjet1_eta;
    std::vector<TH1D> m_Wjet1_phi;
    std::vector<TH1D> m_Wjet2_pt;
    std::vector<TH1D> m_Wjet2_eta;
    std::vector<TH1D> m_Wjet2_phi;
    std::vector<TH1D> m_W_pt;
    std::vector<TH1D> m_W_eta;
    std::vector<TH1D> m_W_phi;
    std::vector<TH1D> m_W_m;

    void FillJetPlots(const Event& event);
    void FillWbosonPlots(const Event& event, const int& index1, const int& index2);
    void FillOtherPlots(const Event& event);
};

#endif
