# TtbarDNN [![build status](https://gitlab.cern.ch/tdado/ttbardnn/badges/master/pipeline.svg "build status")](https://gitlab.cern.ch/tdado/ttbardnn/commits/master)


This code is used to process the ntuples used for ttbar reconstruction using DNNs, produced by [TopRecoEventSaver](https://gitlab.cern.ch/tdado/toprecoeventsaver).
The code produces histograms and various kinds of plots.


## Table of contents
1.  [Getting the code](#getting-the-code)
2.  [Compiling the code](#compiling-the-code)
3.  [Creating file list](#creating-file-list)
4.  [Producing histograms](#producing-histograms)
5.  [Producing plots](#producing-plots)
6.  [Producing inputs for DNN training](#producing-inputs-for-dnn-training)
7.  [Comparing histograms](#comparing-histograms)

## Getting the code

The suggested file structure is as follows:
```
mkdir TtbarDNN
cd TtbarDNN
```
And then get the code usig the following command:
```
git clone ssh://git@gitlab.cern.ch:7999/tdado/ttbardnn.git
```

Then go to the new folder and run the script to make the directory structure
```
./makeDirectoryStructure.sh
```

## Compiling the code
To compile the code you only need `ROOT` and `cmake`.
You can set it up on lxplus via

```
source setup.sh
```

Now compile the code:
```
mkdir build
cd build
cmake ../
make -j4
```

## Creating file list
Now you can produce file list (a text file with the paths to the input files) and also a sumWeights file (a text file with sumWeights, needed for normalisation).
You can do this by going to `scripts/`:
```
cd ../scripts/
```

Change the path to the folder in `makeFileList.sh`.

The folder can contain all files for signal, background and data with any kind of campaign and a mix of FullSim and FastSim samples.
The code fill search for ROOT files in the directory and read the metadata information

Produce the files:
```
./makeFileList.sh
```

This will create the two text files. You can inspect them.

## Producing histograms
Now we have everything ready to process the files in `file_list.txt`.
To produce the nominal (non-systematics) histograms do:

```
cd ../ #To get to the main folder
./build/bin/ttbar-dnn Test
```

The argument (`Test`) can be any string. The code will create a new folder in `../OutputHistos/` with the name that you pass as as argument and put all the output files in the new folder.

You can also process scale factor systematics (systematics done by reweighting) or systematics that need to be read from different tree.
To get scale factor systematics do:

```
./build/bin/ttbar-dnn Test full sfsyst
```

To process the tree systematics do:

```
./build/bin/ttbar-dnn Test full syst
```

The second argument, `full`, tells the code to process all type of files, but you can also specify a single file to process (works also for nominal production), e.g. (`ttbar_PP8_AFII`) via:
```
./build/bin/ttbar-dnn Test ttbar_PP8_AFII syst
```

Note: You first need to run the nominal production to be able to run the systematics!

Note2: When running the nominal processing, a temporary files with `.temp` will be created and only when everything is processes the files will be renamed.
This is done to prevent accidental rewritting of already produced files.

## Producing plots
When at least the nominal production of histograms is finished, you can produce plots by running:
```
./build/bin/plot Test
```

The first argument has to match the name of the produced files from the previous step.
The plots will be stored in `../Plots/` where, again, the folder with the name of the first argument will be created.
You can also pass a third argument `syst` which will tell the code to produce histograms with full systematic error band as specified in `util/plot.cxx` (you need to have the relevant histograms produced).

## Producing inputs for DNN training
To produce the inputs for DNN run:

```
./build/bin/prepare-inputs Test
```

Where the argument is just the name of the folder that will be created in `../InputsDNN/` folder.
The code produces a single ROOT file with two tree: `signal_permutation` and `background_permutation`. The variables are plain `int` and `float`.

## Comparing histograms
You can compare histograms between folders, files and directories inside the ROOT files.
You need to pass 6 arguments to `compare` binary.
The arguments are: folder name, file name, directory in ROOT name, folder name, file name, directory in ROOT name.
The first three arguments will be combined to select one particular set of plots and they will be compared with the second set.
The code checks all histograms and then compares them.
The plots are saved in `../Plots/` folder.

Example usage:
```
./build/bin/compare Pairing_chi500 ttbar_PP8_AFII nominal Pairing_chi500 ttbar_PH7_1_3_AFII nominal
``` 
