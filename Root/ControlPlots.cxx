#include "TtbarDNN/ControlPlots.h"
#include "TtbarDNN/Common.h"
#include "TtbarDNN/Event.h"

#include "TFile.h"
#include "TLorentzVector.h"

#include <iostream>

ControlPlots::ControlPlots() :
    m_isMC(true) {
}

void ControlPlots::Init(const std::vector<std::string>& sf_syst_names) {

    m_sf_syst_names = sf_syst_names;

    for (std::size_t i = 0; i < m_sf_syst_names.size(); ++i) {
        m_jet1_pt.emplace_back("","",50, 0, 1500);
        m_jet1_eta.emplace_back("","",50, 0, 4.5);
        m_jet1_phi.emplace_back("","",50, -3.14, 3.14);
        m_jet2_pt.emplace_back("","",50, 0, 1200);
        m_jet2_eta.emplace_back("","",50, 0, 4.5);
        m_jet2_phi.emplace_back("","",50, -3.14, 3.14);
        m_jet3_pt.emplace_back("","",50, 0, 900);
        m_jet3_eta.emplace_back("","",50, 0, 4.5);
        m_jet3_phi.emplace_back("","",50, -3.14, 3.14);
        m_jet4_pt.emplace_back("","",50, 0, 700);
        m_jet4_eta.emplace_back("","",50, 0, 4.5);
        m_jet4_phi.emplace_back("","",50, -3.14, 3.14);
        m_jet5_pt.emplace_back("","",50, 0, 500);
        m_jet5_eta.emplace_back("","",50, 0, 4.5);
        m_jet5_phi.emplace_back("","",50, -3.14, 3.14);
        m_jet6_pt.emplace_back("","",50, 0, 300);
        m_jet6_eta.emplace_back("","",50, 0, 4.5);
        m_jet6_phi.emplace_back("","",50, -3.14, 3.14);
        m_bjet1_pt.emplace_back("","",50, 0, 1500);
        m_bjet1_eta.emplace_back("","",50, 0, 4.5);
        m_bjet1_phi.emplace_back("","",50, -3.14, 3.14);
        m_bjet2_pt.emplace_back("","",50, 0, 1200);
        m_bjet2_eta.emplace_back("","",50, 0, 4.5);
        m_bjet2_phi.emplace_back("","",50, -3.14, 3.14);
        m_HT.emplace_back("","",50, 0, 2500);
        m_jet_n.emplace_back("","",6, 3.5, 9.5);
        m_btag_bin.emplace_back("","",5, 0.5, 5.5);
        
        m_el_pt.emplace_back("","",50, 0, 500);
        m_el_eta.emplace_back("","",50, 0, 2.5);
        m_el_phi.emplace_back("","",50, -3.14, 3.14);
        m_mu_pt.emplace_back("","",50, 0, 500);
        m_mu_eta.emplace_back("","",50, 0, 2.5);
        m_mu_phi.emplace_back("","",50, -3.14, 3.14);
        m_met.emplace_back("","",50, 0, 500);
        m_met_phi.emplace_back("","",50, -3.14, 3.14);
        m_average_mu.emplace_back("","",50, 0, 100);
        m_actual_mu.emplace_back("","",50, 0, 100);
        
        m_Wjet1_pt.emplace_back("","",50, 0, 1000);
        m_Wjet1_eta.emplace_back("","",50, 0, 4.5);
        m_Wjet1_phi.emplace_back("","",50, -3.14, 3.14);
        m_Wjet2_pt.emplace_back("","",50, 0, 1000);
        m_Wjet2_eta.emplace_back("","",50, 0, 4.5);
        m_Wjet2_phi.emplace_back("","",50, -3.14, 3.14);
        m_W_pt.emplace_back("","",50, 0, 1000);
        m_W_eta.emplace_back("","",50, 0, 5);
        m_W_phi.emplace_back("","",50, -3.14, 3.14);
        m_W_m.emplace_back("","",50, 0, 250);
    }
}

void ControlPlots::FillAllHistos(const Event& event,
                                 const int& index1,
                                 const int& index2,
                                 const bool& isMC,
                                 const std::vector<double>& weights) {

    m_isMC = isMC;
    m_weights = weights;

    FillJetPlots(event);

    FillWbosonPlots(event, index1, index2);

    FillOtherPlots(event);
}

void ControlPlots::Finalise() {
    for (std::size_t i = 0; i < m_weights.size(); ++i) {
        Common::AddOverflow(&m_jet1_pt.at(i));
        Common::AddOverflow(&m_jet1_eta.at(i));
        Common::AddOverflow(&m_jet1_phi.at(i));
        Common::AddOverflow(&m_jet2_pt.at(i));
        Common::AddOverflow(&m_jet2_eta.at(i));
        Common::AddOverflow(&m_jet2_phi.at(i));
        Common::AddOverflow(&m_jet3_pt.at(i));
        Common::AddOverflow(&m_jet3_eta.at(i));
        Common::AddOverflow(&m_jet3_phi.at(i));
        Common::AddOverflow(&m_jet4_pt.at(i));
        Common::AddOverflow(&m_jet4_eta.at(i));
        Common::AddOverflow(&m_jet4_phi.at(i));
        Common::AddOverflow(&m_jet5_pt.at(i));
        Common::AddOverflow(&m_jet5_eta.at(i));
        Common::AddOverflow(&m_jet5_phi.at(i));
        Common::AddOverflow(&m_jet6_pt.at(i));
        Common::AddOverflow(&m_jet6_eta.at(i));
        Common::AddOverflow(&m_jet6_phi.at(i));
        Common::AddOverflow(&m_bjet1_pt.at(i));
        Common::AddOverflow(&m_bjet1_eta.at(i));
        Common::AddOverflow(&m_bjet1_phi.at(i));
        Common::AddOverflow(&m_bjet2_pt.at(i));
        Common::AddOverflow(&m_bjet2_eta.at(i));
        Common::AddOverflow(&m_bjet2_phi.at(i));
        Common::AddOverflow(&m_HT.at(i));
        Common::AddOverflow(&m_jet_n.at(i));
        Common::AddOverflow(&m_btag_bin.at(i));
        
        Common::AddOverflow(&m_el_pt.at(i));
        Common::AddOverflow(&m_el_eta.at(i));
        Common::AddOverflow(&m_el_phi.at(i));
        Common::AddOverflow(&m_mu_pt.at(i));
        Common::AddOverflow(&m_mu_eta.at(i));
        Common::AddOverflow(&m_mu_phi.at(i));
        Common::AddOverflow(&m_met.at(i));
        Common::AddOverflow(&m_met_phi.at(i));
        Common::AddOverflow(&m_average_mu.at(i));
        Common::AddOverflow(&m_actual_mu.at(i));
        
        Common::AddOverflow(&m_Wjet1_pt.at(i));
        Common::AddOverflow(&m_Wjet1_eta.at(i));
        Common::AddOverflow(&m_Wjet1_phi.at(i));
        Common::AddOverflow(&m_Wjet2_pt.at(i));
        Common::AddOverflow(&m_Wjet2_eta.at(i));
        Common::AddOverflow(&m_Wjet2_phi.at(i));
        Common::AddOverflow(&m_W_pt.at(i));
        Common::AddOverflow(&m_W_eta.at(i));
        Common::AddOverflow(&m_W_phi.at(i));
        Common::AddOverflow(&m_W_m.at(i));
    }
}

void ControlPlots::Write(const std::string& name, TFile* file) {
    for (std::size_t i = 0; i < m_sf_syst_names.size(); ++i) {
        if (m_sf_syst_names.size() == 1) {
            file->cd(name.c_str());
        } else {
            file->cd(m_sf_syst_names.at(i).c_str());
        }
        m_jet1_pt.at(i).Write("jet1_pt");
        m_jet1_eta.at(i).Write("jet1_eta");
        m_jet1_phi.at(i).Write("jet1_phi");
        m_jet2_pt.at(i).Write("jet2_pt");
        m_jet2_eta.at(i).Write("jet2_eta");
        m_jet2_phi.at(i).Write("jet2_phi");
        m_jet3_pt.at(i).Write("jet3_pt");
        m_jet3_eta.at(i).Write("jet3_eta");
        m_jet3_phi.at(i).Write("jet3_phi");
        m_jet4_pt.at(i).Write("jet4_pt");
        m_jet4_eta.at(i).Write("jet4_eta");
        m_jet4_phi.at(i).Write("jet4_phi");
        m_jet5_pt.at(i).Write("jet5_pt");
        m_jet5_eta.at(i).Write("jet5_eta");
        m_jet5_phi.at(i).Write("jet5_phi");
        m_jet6_pt.at(i).Write("jet6_pt");
        m_jet6_eta.at(i).Write("jet6_eta");
        m_jet6_phi.at(i).Write("jet6_phi");
        m_bjet1_pt.at(i).Write("bjet1_pt");
        m_bjet1_eta.at(i).Write("bjet1_eta");
        m_bjet1_phi.at(i).Write("bjet1_phi");
        m_bjet2_pt.at(i).Write("bjet2_pt");
        m_bjet2_eta.at(i).Write("bjet2_eta");
        m_bjet2_phi.at(i).Write("bjet2_phi");
        m_HT.at(i).Write("HT");
        m_jet_n.at(i).Write("jet_n");
        m_btag_bin.at(i).Write("btag_bin");
        
        m_el_pt.at(i).Write("el_pt");
        m_el_eta.at(i).Write("el_eta");
        m_el_phi.at(i).Write("el_phi");
        m_mu_pt.at(i).Write("mu_pt");
        m_mu_eta.at(i).Write("mu_eta");
        m_mu_phi.at(i).Write("mu_phi");
        m_met.at(i).Write("met");
        m_met_phi.at(i).Write("met_phi");
        m_average_mu.at(i).Write("average_mu");
        m_actual_mu.at(i).Write("actual_mu");
        
        m_Wjet1_pt.at(i).Write("Wjet1_pt");
        m_Wjet1_eta.at(i).Write("Wjet1_eta");
        m_Wjet1_phi.at(i).Write("Wjet1_phi");
        m_Wjet2_pt.at(i).Write("Wjet2_pt");
        m_Wjet2_eta.at(i).Write("Wjet2_eta");
        m_Wjet2_phi.at(i).Write("Wjet2_phi");
        m_W_pt.at(i).Write("W_pt");
        m_W_eta.at(i).Write("W_eta");
        m_W_phi.at(i).Write("W_phi");
        m_W_m.at(i).Write("W_m");
    }
}
    
void ControlPlots::FillJetPlots(const Event& event) {
    for (std::size_t i = 0; i < m_weights.size(); ++i) {
        m_jet1_pt.at(i).Fill(event.jet_pt->at(0)/1e3, m_weights.at(i));
        m_jet1_eta.at(i).Fill(std::fabs(event.jet_eta->at(0)), m_weights.at(i));
        m_jet1_phi.at(i).Fill(event.jet_phi->at(0), m_weights.at(i));
        m_jet2_pt.at(i).Fill(event.jet_pt->at(1)/1e3, m_weights.at(i));
        m_jet2_eta.at(i).Fill(std::fabs(event.jet_eta->at(1)), m_weights.at(i));
        m_jet2_phi.at(i).Fill(event.jet_phi->at(1), m_weights.at(i));
        m_jet3_pt.at(i).Fill(event.jet_pt->at(2)/1e3, m_weights.at(i));
        m_jet3_eta.at(i).Fill(std::fabs(event.jet_eta->at(2)), m_weights.at(i));
        m_jet3_phi.at(i).Fill(event.jet_phi->at(2), m_weights.at(i));
        m_jet4_pt.at(i).Fill(event.jet_pt->at(3)/1e3, m_weights.at(i));
        m_jet4_eta.at(i).Fill(std::fabs(event.jet_eta->at(3)), m_weights.at(i));
        m_jet4_phi.at(i).Fill(event.jet_phi->at(3), m_weights.at(i));
        if (event.jet_pt->size() > 4) {
            m_jet5_pt.at(i).Fill(event.jet_pt->at(4)/1e3, m_weights.at(i));
            m_jet5_eta.at(i).Fill(std::fabs(event.jet_eta->at(4)), m_weights.at(i));
            m_jet5_phi.at(i).Fill(event.jet_phi->at(4), m_weights.at(i));
        }
        if (event.jet_pt->size() > 5) {
            m_jet6_pt.at(i).Fill(event.jet_pt->at(5)/1e3, m_weights.at(i));
            m_jet6_eta.at(i).Fill(std::fabs(event.jet_eta->at(5)), m_weights.at(i));
            m_jet6_phi.at(i).Fill(event.jet_phi->at(5), m_weights.at(i));
        }
        m_jet_n.at(i).Fill(event.jet_pt->size(), m_weights.at(i));

        /// btagging
        std::size_t nbjets(0);
        for (std::size_t ijet = 0; ijet < event.jet_pt->size(); ++ijet) {
            m_btag_bin.at(i).Fill(event.jet_tagWeightBin_DL1r_Continuous->at(ijet), m_weights.at(i));
            if (event.jet_tagWeightBin_DL1r_Continuous->at(ijet) < 5) continue;
            nbjets++;
            if (nbjets == 1) {
                m_bjet1_pt.at(i).Fill(event.jet_pt->at(ijet)/1e3, m_weights.at(i));
                m_bjet1_eta.at(i).Fill(std::fabs(event.jet_eta->at(ijet)), m_weights.at(i));
                m_bjet1_phi.at(i).Fill(event.jet_phi->at(ijet), m_weights.at(i));
            }
            if (nbjets == 2) {
                m_bjet2_pt.at(i).Fill(event.jet_pt->at(ijet)/1e3, m_weights.at(i));
                m_bjet2_eta.at(i).Fill(std::fabs(event.jet_eta->at(ijet)), m_weights.at(i));
                m_bjet2_phi.at(i).Fill(event.jet_phi->at(ijet), m_weights.at(i));
            }
        }

        /// HT
        float ht(0.);
        for (const auto ipt : *event.jet_pt) {
            ht += ipt/1e3;
        }
        m_HT.at(i).Fill(ht, m_weights.at(i));
    }
}

void ControlPlots::FillWbosonPlots(const Event& event,
                                   const int& index1,
                                   const int& index2) {
    TLorentzVector j1, j2, W;
    j1.SetPtEtaPhiE(event.jet_pt->at(index1)/1e3,
                    event.jet_eta->at(index1),
                    event.jet_phi->at(index1),
                    event.jet_e->at(index1)/1e3);
    j2.SetPtEtaPhiE(event.jet_pt->at(index2)/1e3,
                    event.jet_eta->at(index2),
                    event.jet_phi->at(index2),
                    event.jet_e->at(index2)/1e3);

    W = j1 + j2;

    for (std::size_t i = 0; i < m_weights.size(); ++i) {
        m_Wjet1_pt.at(i).Fill(j1.Pt(), m_weights.at(i));
        m_Wjet1_eta.at(i).Fill(std::fabs(j1.Eta()), m_weights.at(i));
        m_Wjet1_phi.at(i).Fill(j1.Phi(), m_weights.at(i));
        m_Wjet2_pt.at(i).Fill(j2.Pt(), m_weights.at(i));
        m_Wjet2_eta.at(i).Fill(std::fabs(j2.Eta()), m_weights.at(i));
        m_Wjet2_phi.at(i).Fill(j2.Phi(), m_weights.at(i));

        m_W_pt.at(i).Fill(W.Pt(), m_weights.at(i));
        m_W_eta.at(i).Fill(std::fabs(W.Eta()), m_weights.at(i));
        m_W_phi.at(i).Fill(W.Phi(), m_weights.at(i));
        m_W_m.at(i).Fill(W.M(), m_weights.at(i));
    }

}

void ControlPlots::FillOtherPlots(const Event& event) {
    bool is_el(false);

    if (event.ejets_2015 || event.ejets_2016 || event.ejets_2017 || event.ejets_2018) is_el = true;
    else if (event.mujets_2015 || event.mujets_2016 || event.mujets_2017 || event.mujets_2018) is_el = false;
    else {
        std::cerr << "ControlPlots::FillWbosonPlots: Event is not el nor mu... skipping" << std::endl;
        return;
    }

    /// leptons
    for (std::size_t i = 0; i < m_weights.size(); ++i) {
        if (is_el) {
            m_el_pt.at(i).Fill(event.el_pt->at(0)/1e3, m_weights.at(i));
            m_el_eta.at(i).Fill(std::fabs(event.el_eta->at(0)), m_weights.at(i));
            m_el_phi.at(i).Fill(event.el_phi->at(0), m_weights.at(i));
        } else {
            m_mu_pt.at(i).Fill(event.mu_pt->at(0)/1e3, m_weights.at(i));
            m_mu_eta.at(i).Fill(std::fabs(event.mu_eta->at(0)), m_weights.at(i));
            m_mu_phi.at(i).Fill(event.mu_phi->at(0), m_weights.at(i));
        } 
    
        /// MET
        m_met.at(i).Fill(event.met_met/1e3, m_weights.at(i));
        m_met_phi.at(i).Fill(event.met_phi, m_weights.at(i));

        /// average/actual mu
        if (m_isMC) {
            m_average_mu.at(i).Fill(event.mu, m_weights.at(i));
            m_actual_mu.at(i).Fill(event.mu_actual, m_weights.at(i));
        } else {
            m_average_mu.at(i).Fill(event.mu/1.03, m_weights.at(i));
            m_actual_mu.at(i).Fill(event.mu_actual/1.03, m_weights.at(i));
        }
    }
}
