#include "TtbarDNN/DnnPreparator.h"
#include "TtbarDNN/Event.h"
#include "TtbarDNN/DnnTree.h"
#include "TtbarDNN/WeightManager.h"
#include "Xsection/SampleXsection.h"

#include "TChain.h"

#include <fstream>
#include <iostream>
#include <memory>

DnnPreparator::DnnPreparator(const std::string& name) :
    m_name(name) {    
}

void DnnPreparator::ReadConfig() {
    ReadFileList("scripts/file_list.txt");
}

void DnnPreparator::ProcessFiles() {


    /// Get the cross-section tool
    SampleXsection xSecTool{};
    if (!xSecTool.readFromFile("data/XSection-MC15-13TeV.data")) {
        std::cerr << "DnnPreparator::ProcessFiles: Cannot open data file with cross-sections" << std::endl;
        exit(EXIT_FAILURE);
    }

    /// Get the WeightManager tool
    WeightManager weight_manager{};

    if (!weight_manager.ReadFile("scripts/sumW.txt")) {
        std::cerr << "DnnPreparator::ProcessFiles: Cannot read the file with sum weights" << std::endl;
        exit(EXIT_FAILURE);
    }

    std::unique_ptr<TChain> chain(new TChain("nominal"));

    for (const auto& ifile : m_file_path) {
        chain->Add(ifile.c_str());
    }

    const int nEvents = chain->GetEntries();


    /// calculate Xsection
    const float xSec = xSecTool.getXsection(410470);
    if (xSec < 0 ) {
        std::cerr << "DnnPreparator::ProcessFiles: Negative cross-section found, skipping the file" << std::endl;
        exit(EXIT_FAILURE);
    }
    static constexpr float lumi_1516 = 32988.1 + 3219.56; 
    //static constexpr float lumi_17 = 44307.4; 
    //static constexpr float lumi_18 = 58450.1;

    weight_manager.SetDsidAf2CampaignType(410470, "FS", "mc16a", "ttbar_PP8_FS");
    weight_manager.SetXsecLumi(xSec, lumi_1516);
    weight_manager.ProcessNominalNormalisation();

    DnnTree dnn_tree(m_name);
    
    /// Event (tree, isMC, isTT, useLoose, isSyst)
    Event event(chain.get(), true, true, false, false);

    for (int ievent = 0; ievent < nEvents; ++ievent) {
        event.GetEntry(ievent);
        if (ievent % 100000 == 0) {
            std::cout << "\t\tProcessing event " << ievent << ", out of " << nEvents << " events\n";
        }

        const float weight = weight_manager.GetNominalWeight(event);

        dnn_tree.ProcessSingleEvent(event, weight);
    }

    dnn_tree.Write();
}

void DnnPreparator::ReadFileList(const std::string& path) {
    std::cout << "\nDnnPreparator::ReadFileList: Reading file list from: " << path << std::endl;
    std::string file_path, file_type, af2, campaign;
    int dsid;

    /// Open file
    std::fstream file(path.c_str(), std::ios_base::in);

    if (!file.is_open()) {
        std::cerr << "DnnPreparator::ReadFileList: Cannot open the file list at: " << path << std::endl;
        exit(EXIT_FAILURE);
    }

    while (file >> file_path >> file_type >> af2 >> dsid >> campaign) {
        if (file_type != "ttbar_PP8_FS") continue;
        if (campaign != "mc16a") continue;
        m_file_path.emplace_back(file_path);
        m_campaign.emplace_back(campaign);
    }

    if (m_file_path.size() == 0) {
        std::cerr << "DnnPreparator::ReadFileList: File list is empty?" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (m_file_path.size() != m_campaign.size()) {
        std::cerr << "DnnPreparator::ReadFileList: Size of file_path and campaign does not match!" << std::endl;
        exit(EXIT_FAILURE);
    }

    std::cout << "\nDnnPreparator::ReadFileList: File list read successfully \n\n";
}
