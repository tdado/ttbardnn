#include "TtbarDNN/WeightManager.h"
#include "TtbarDNN/Event.h"

#include <algorithm>
#include <fstream>
#include <iostream>

WeightManager::WeightManager() :
    m_dsid(-1),
    m_af2(""),
    m_campaign(""),
    m_xSec(-1),
    m_lumi(-1),
    m_nominal_normalisation(-1),
    m_type("") {

    InitialiseModelNames();
    m_sf_enum = SfSyst::GetAllSFs();
}

bool WeightManager::ReadFile(const std::string& path) {
    std::cout << "\nWeightManager::ReadFile: Reading sumWeights file from: " << path << "\n";
    std::string af2, campaign, name;
    int dsid, index;
    double sumw;

    std::fstream file(path.c_str(), std::ios_base::in);

    if (!file.is_open()) {
        std::cerr << "WeightManager::ReadFile: Cannot open file for sumWeights" << std::endl;
        return false;
    }

    while (file >> dsid >> af2 >> campaign >> index >> name >> sumw) {
        const std::string key = std::to_string(dsid) + af2 + campaign;
        auto itr = m_weight_map.find(key);

        /// does not exist in the map
        if (itr == m_weight_map.end()) {
            WeightManager::Weight weight;
            weight.names.emplace_back(name);
            weight.sumW.emplace_back(sumw);
            weight.indices.emplace_back(index);
            m_weight_map[key] = weight;
        } else { /// already exist
            m_weight_map[key].names.emplace_back(name);
            m_weight_map[key].sumW.emplace_back(sumw);
            m_weight_map[key].indices.emplace_back(index);
        }
    }

    m_weight_buggy["fsrRedHi"] = "Var3cUp";
    m_weight_buggy["fsrRedLo"] = "Var3cDown";
    m_weight_buggy["fsrDefHi"] = "isrPDFplus";
    m_weight_buggy["fsrDefLo"] = "isrPDFminus";
    m_weight_buggy["fsrConHi"] = "isrRedHi";
    m_weight_buggy["fsrConLo"] = "fsrRedHi";
    m_weight_buggy["fsr_G2GG_muR_dn"] = "isrRedLo";
    m_weight_buggy["fsr_G2GG_muR_up"] = "fsrRedLo";
    m_weight_buggy["fsr_G2QQ_muR_dn"] = "isrDefHi";
    m_weight_buggy["fsr_G2QQ_muR_up"] = "fsrDefHi";
    m_weight_buggy["fsr_Q2QG_muR_dn"] = "isrDefLo";
    m_weight_buggy["fsr_Q2QG_muR_up"] = "fsrDefLo";
    m_weight_buggy["fsr_X2XG_muR_dn"] = "isrConHi";
    m_weight_buggy["fsr_X2XG_muR_up"] = "fsrConHi";
    m_weight_buggy["fsr_G2GG_cNS_dn"] = "isrConLo";
    m_weight_buggy["fsr_G2GG_cNS_up"] = "fsrConLo";
    m_weight_buggy["fsr_G2QQ_cNS_dn"] = "fsr_G2GG_muR_dn";
    m_weight_buggy["fsr_G2QQ_cNS_up"] = "fsr_G2GG_muR_up";
    m_weight_buggy["fsr_Q2QG_cNS_dn"] = "fsr_G2QQ_muR_dn";
    m_weight_buggy["fsr_Q2QG_cNS_up"] = "fsr_G2QQ_muR_up";
    m_weight_buggy["fsr_X2XG_cNS_dn"] = "fsr_Q2QG_muR_dn";
    m_weight_buggy["fsr_X2XG_cNS_up"] = "fsr_Q2QG_muR_up";
    m_weight_buggy["Var3cUp"] = "fsr_X2XG_muR_dn";
    m_weight_buggy["Var3cDown"] = "fsr_X2XG_muR_up";
    m_weight_buggy["isrPDFplus"] = "fsr_G2GG_cNS_dn";
    m_weight_buggy["isrPDFminus"] = "fsr_G2GG_cNS_up";
    m_weight_buggy["isrRedHi"] = "fsr_G2QQ_cNS_dn";
    m_weight_buggy["isrRedLo"] = "fsr_G2QQ_cNS_up";
    m_weight_buggy["isrDefHi"] = "fsr_Q2QG_cNS_dn";
    m_weight_buggy["isrDefLo"] = "fsr_Q2QG_cNS_up";
    m_weight_buggy["isrConHi"] = "fsr_X2XG_cNS_dn";
    m_weight_buggy["isrConLo"] = "fsr_X2XG_cNS_up";

    file.close();

    std::cout << "\nWeightManager::ReadFile: Finished reading the sumWeight file\n";

    return true;
}

void WeightManager::SetDsidAf2CampaignType(const int& dsid,
                                           const std::string& af2,
                                           const std::string& campaign,
                                           const std::string& type) {
    m_dsid = dsid;
    m_af2 = af2;
    m_campaign = campaign;
    m_type = type;
}

void WeightManager::SetXsecLumi(const double& xSec,
                                const double& lumi) {
    m_xSec = xSec;
    m_lumi = lumi;
}

double WeightManager::GetNominalWeight(const Event& event) const {
    return m_nominal_normalisation*event.weight_mc*event.weight_pileup*event.weight_leptonSF*event.weight_bTagSF_DL1r_Continuous*event.weight_jvt;
}

std::vector<double> WeightManager::GetSfSystWeights(const Event& event) const {
    std::vector<double> result;

    const double nominal = GetNominalWeight(event);

    /// SF syst
    for (const auto& isf : m_sf_enum) {
        const double wei = SfSyst::EnumToWeight(isf, event);
        if (wei < -10000) {
            std::cerr << "WeightManager::GetSfSystWeights: No function setup to calculate this variation of SFs." << std::endl;
            exit(EXIT_FAILURE);
        }

        result.emplace_back(nominal*wei);
    }

    std::vector<std::string> model_names;
    auto itr = m_model_sf_names.find(m_type);
    if (itr != m_model_sf_names.end()) {
        model_names = itr->second;
    } else {
        return result;
    }

    const std::vector<std::size_t> model_indices = (m_type == "ttbar_PP8_shower_decor_AFII") ? WeightManager::GetModelIndicesFromBuggyPythia(model_names) : WeightManager::GetModelIndices(model_names);
    for (std::size_t i = 0; i < model_indices.size(); ++i) {
        const double wei = m_model_normalisation.at(i)*event.mc_generator_weights->at(model_indices.at(i))*event.weight_pileup*event.weight_leptonSF*event.weight_bTagSF_DL1r_Continuous*event.weight_jvt;
        if (std::fabs(wei) > 50) {
            result.emplace_back(1.);
        } else {
            result.emplace_back(wei);
        }
    }
    

    return result;
}

void WeightManager::ProcessNominalNormalisation() {
    if (m_dsid < 0 || m_af2 == "" || m_campaign == "" || m_type == "") {
        std::cerr << "WeightManager::ProcessNominalNormalisation: DSID, AF2 or campaign not set. Setting normalisation to -1" << std::endl;
        m_nominal_normalisation = -1;
    }

    if (m_xSec < 0 || m_lumi < 0) {
        std::cerr << "WeightManager::ProcessNominalNormalisation: Cross-section or luminosity not set. Setting normalisation to -1" << std::endl;
        m_nominal_normalisation = -1;
    }

    const double sumW = GetNominalSumWeight();
    m_nominal_normalisation = m_xSec * m_lumi / sumW;
}

void WeightManager::ProcessModellingNormalisation() {
    if (m_dsid < 0 || m_af2 == "" || m_campaign == "" || m_type == "") {
        std::cerr << "WeightManager::ProcessModellingNormalisation: DSID, AF2 or campaign not set." << std::endl;
        exit(EXIT_FAILURE);
    }

    if (m_xSec < 0 || m_lumi < 0) {
        std::cerr << "WeightManager::ProcessModellingNormalisation: Cross-section or luminosity not set." << std::endl;
        exit(EXIT_FAILURE);
    }

    /// get the list of indices for modelling sf systs
    const WeightManager::Weight weight = FindWeight();

    std::vector<std::string> model_names;
    auto itr = m_model_sf_names.find(m_type);
    if (itr != m_model_sf_names.end()) {
        model_names = itr->second;
    } else {
        return;
    }

    const std::vector<std::size_t> model_indices = (m_type == "ttbar_PP8_shower_decor_AFII") ? GetModelIndicesFromBuggyPythia(model_names) : GetModelIndices(model_names);

    for (const auto& i : model_indices) {
        if (i >= weight.sumW.size()) {
            std::cerr << "WeightManager::ProcessModellingNormalisation: Illegal index: " << i  << " for the modelling uncertainty for DSID: " << m_dsid << ", ignoring" << std::endl;
            continue;
        }

        const double normalisation = m_xSec * m_lumi / weight.sumW.at(i);
        m_model_normalisation.emplace_back(normalisation);
        if (model_names.size() != 0) {
            if (std::find(model_names.begin(), model_names.end(), weight.names.at(i)) == model_names.end()) {
                std::cerr << "WeightManager::ProcessModellingNormalisation: The name of the sf syst modellig variation is not properly set: " << weight.names.at(i) << std::endl;
                exit(EXIT_FAILURE);
            }
        }
    }
}

std::vector<std::string> WeightManager::GetSfSystList(const std::string& systematics,
                                                      const std::string& type) const {
    std::vector<std::string> result;

    if (systematics == "sfsyst") {
        for (const auto& isf : m_sf_enum) {
            const std::string name = SfSyst::EnumToString(isf);
            if (name == "") {
                std::cerr << "WeightManager::GetSfSystList: Wrong Sfsyst enum!" << std::endl;
                exit(EXIT_FAILURE);
            }
            result.emplace_back(name);
        }
    } else {
        result.emplace_back("nominal");
    }

    if (systematics == "sfsyst") {
        /// Add Modelling
        auto itr = m_model_sf_names.find(type);
        if (itr != m_model_sf_names.end()) {
            result.insert(result.end(), itr->second.begin(), itr->second.end());
        }
    }

    return result;
}

void WeightManager::Clear() {
    m_nominal_normalisation = -1;
    m_model_normalisation.clear();
}

double WeightManager::GetNominalSumWeight() const {

    const WeightManager::Weight weight = FindWeight();
    if (weight.names.size() == 1) {
        std::cout << "\t\tWeightManager::GetNominalSumWeight: Only one sumW, returning it\n";
        return weight.sumW.at(0);
    }
    static const std::vector<std::string> key_names = {"nominal","Default","1001","Weight","muR010000E+01muF010000E+01"};
    std::size_t position(9999);

    auto itr = std::find(weight.names.begin(), weight.names.end(), key_names.at(0));
    if (itr != weight.names.end()) {
       position = std::distance(weight.names.begin(), itr); 
    } else {
        std::cout << "\t\tWeightManager::GetNominalSumWeight: Weight " << key_names.at(0) << " not found\n";
        for (std::size_t ialt = 1; ialt < key_names.size(); ++ialt) {
            std::cout << "\t\tWeightManager::GetNominalSumWeight: Trying weight " << key_names.at(ialt) << "\n";
            itr = std::find(weight.names.begin(), weight.names.end(), key_names.at(ialt));
            if (itr != weight.names.end()) {
                position = std::distance(weight.names.begin(), itr); 
                break;
            }
        }
    }
    
    if (itr == weight.names.end()) {
        std::cerr << "WeightManager::GetNominalSumWeight: Cannot find the nominal weight!" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (position != 0) {
        std::cout << "\n\t\tWeightManager::GetNominalWeight: The nominal weight is not the first one but : " << position <<  "\n";
    }

    return weight.sumW.at(position);
}

WeightManager::Weight WeightManager::FindWeight() const {

    const std::string key = std::to_string(m_dsid)+m_af2+m_campaign;
    auto itr = m_weight_map.find(key);

    if (itr == m_weight_map.end()) {
        std::cerr << "WeightManager::FindWeightVector: Cannot find DSID: " << m_dsid << ", af2: " << m_af2 << ", campaign: " << m_campaign << ", in the map" << std::endl;
        exit(EXIT_FAILURE);
    }

    return itr->second;
}

void WeightManager::InitialiseModelNames() {
    
    /// ttbar
    m_model_sf_names["ttbar_PP8_FS"] = {"muR10muF20","muR10muF05","muR20muF10","muR05muF10","Var3cUp","Var3cDown","isrmuRfac10_fsrmuRfac20","isrmuRfac10_fsrmuRfac05","PDFset90900"};
    m_model_sf_names["ttbar_PP8_AFII"] = {"muR10muF20","muR10muF05","muR20muF10","muR05muF10","Var3cUp","Var3cDown","isrmuRfac10_fsrmuRfac20","isrmuRfac10_fsrmuRfac05","PDFset90900"};
    m_model_sf_names["ttbar_PP8_dilep_FS"] = {"muR10muF20","muR10muF05","muR20muF10","muR05muF10","Var3cUp","Var3cDown","isrmuRfac10_fsrmuRfac20","isrmuRfac10_fsrmuRfac05","PDFset90900"};

    m_model_sf_names["ttbar_PP8_shower_decor_AFII"] = {"Var3cUp","Var3cDown",
                                                       "isrPDFplus","isrPDFminus","isrRedHi","fsrRedHi","isrRedLo","fsrRedLo","isrDefHi","fsrDefHi","isrDefLo","fsrDefLo","isrConHi",
                                                       "fsrConHi","isrConLo","fsrConLo","fsr_G2GG_muR_dn","fsr_G2GG_muR_up","fsr_G2QQ_muR_dn","fsr_G2QQ_muR_up","fsr_Q2QG_muR_dn",
                                                       "fsr_Q2QG_muR_up","fsr_X2XG_muR_dn","fsr_X2XG_muR_up","fsr_G2GG_cNS_dn","fsr_G2GG_cNS_up","fsr_G2QQ_cNS_dn", "fsr_G2QQ_cNS_up",
                                                       "fsr_Q2QG_cNS_dn","fsr_Q2QG_cNS_up","fsr_X2XG_cNS_dn","fsr_X2XG_cNS_up","isr_G2GG_muR_dn","isr_G2GG_muR_up","isr_G2QQ_muR_dn",
                                                       "isr_G2QQ_muR_up","isr_Q2QG_muR_dn","isr_Q2QG_muR_up","isr_X2XG_muR_dn","isr_X2XG_muR_up","isr_G2GG_cNS_dn","isr_G2GG_cNS_up",
                                                       "isr_G2QQ_cNS_dn","isr_G2QQ_cNS_up","isr_Q2QG_cNS_dn","isr_Q2QG_cNS_up","isr_X2XG_cNS_dn", "isr_X2XG_cNS_up"};
    
    /// single top
    m_model_sf_names["SingleTop_PP8_s_chan_FS"] = {"muR100muF200","muR100muF050","muR200muF100","muR050muF100","Var3cUp","Var3cDown","isrmuRfac10_fsrmuRfac20","isrmuRfac10_fsrmuRfac05"};
    m_model_sf_names["SingleTop_PP8_t_chan_FS"] = {"muR100muF200","muR100muF050","muR200muF100","muR050muF100","Var3cUp","Var3cDown","isrmuRfac10_fsrmuRfac20","isrmuRfac10_fsrmuRfac05"};
    m_model_sf_names["SingleTop_PP8_tW_chan_FS"] = {"muR100muF200","muR100muF050","muR200muF100","muR050muF100","Var3cUp","Var3cDown","isrmuRfac10_fsrmuRfac20","isrmuRfac10_fsrmuRfac05"};
    m_model_sf_names["SingleTop_PP8_tW_chan_dilep_FS"] = {"muR100muF200","muR100muF050","muR200muF100","muR050muF100","Var3cUp","Var3cDown","isrmuRfac10_fsrmuRfac20","isrmuRfac10_fsrmuRfac05"};

    /// PDFs
    for (int i = 90901; i <= 90930; ++i) {
        m_model_sf_names["ttbar_PP8_FS"].emplace_back("PDFset"+std::to_string(i));
        m_model_sf_names["ttbar_PP8_AFII"].emplace_back("PDFset"+std::to_string(i));
    }
}

std::vector<std::size_t> WeightManager::GetModelIndices(const std::vector<std::string>& names) const {
    std::vector<std::size_t> result;

    const WeightManager::Weight weight = FindWeight();
    for (const auto& isf : names) {
        auto itr = std::find(weight.names.begin(), weight.names.end(), isf);
        if (itr == weight.names.end()) {
            std::cerr << "WeightManager::GetModelIndices: Cannot find weight : " << isf << " in the sum weights file for DSID : " << m_dsid << std::endl;
            exit(EXIT_FAILURE);
        }
        result.emplace_back(std::distance(weight.names.begin(), itr));
    }

    return result;
}

std::vector<std::size_t> WeightManager::GetModelIndicesFromBuggyPythia(const std::vector<std::string>& names) const {
    std::vector<std::size_t> result;
    const WeightManager::Weight weight = FindWeight();
    
    for (const auto& isf : names) {
        auto itr = std::find(weight.names.begin(), weight.names.end(), isf);
        if (itr == weight.names.end()) {
            std::cerr << "WeightManager::GetModelIndicesFromBuggyPythia: Cannot find weight : " << isf << " in the sum weights file for DSID : " << m_dsid << std::endl;
            exit(EXIT_FAILURE);
        }

        /// now pass it through the dictionary
        auto map_itr = m_weight_buggy.find(isf);
        if (map_itr == m_weight_buggy.end()) {
            result.emplace_back(std::distance(weight.names.begin(), itr));
        } else {
            auto tmp_itr = std::find(weight.names.begin(), weight.names.end(), map_itr->second);
            if (tmp_itr == weight.names.end()) {
                std::cerr << "WeightManager::GetModelIndicesFromBuggyPythia: Cannot find the dictionary weight: " << map_itr->second << " in the sum weight" << std::endl;
                exit(EXIT_FAILURE);
            }
            
            result.emplace_back(std::distance(weight.names.begin(), tmp_itr));
        }
    }

    return result;
}
