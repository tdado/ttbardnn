#include "TtbarDNN/DnnTree.h"
#include "TtbarDNN/Event.h"

#include "TLorentzVector.h"
#include "TSystem.h"

#include <algorithm>
#include <iostream>

DnnTree::DnnTree(const std::string& name) :
    m_name(name),
    m_random(new TRandom3(0)),
    m_weight(-1),
    m_jet1_pt(-1),
    m_jet1_eta(-1),
    m_jet1_phi(-1),
    m_jet1_e(-1),
    m_jet1_tag_bin(-1),
    m_jet2_pt(-1),
    m_jet2_eta(-1),
    m_jet2_phi(-1),
    m_jet2_e(-1),
    m_jet2_tag_bin(-1),
    m_jet3_pt(-1),
    m_jet3_eta(-1),
    m_jet3_phi(-1),
    m_jet3_e(-1),
    m_jet3_tag_bin(-1),
    m_jet4_pt(-1),
    m_jet4_eta(-1),
    m_jet4_phi(-1),
    m_jet4_e(-1),
    m_jet4_tag_bin(-1),
    m_jet_n(-1),
    m_had_top_pt(-1),
    m_had_top_eta(-1),
    m_had_top_phi(-1),
    m_had_top_m(-1),
    m_had_W_pt(-1),
    m_had_W_eta(-1),
    m_had_W_phi(-1),
    m_had_W_m(-1),
    m_W_jets_dR(-1),
    m_b_lep_dR(-1),
    m_mlb(-1),
    m_pt_lb(-1) {

    gSystem->mkdir(("../InputDNN/"+name).c_str());

    m_file = std::unique_ptr<TFile>(new TFile(("../InputDNN/"+name+"/InputFile.root").c_str(), "RECREATE"));
    if (!m_file) {
        std::cerr << "DnnTree::DnnTree: Cannot open output file";
        exit(EXIT_FAILURE);
    }

    m_file->cd();

    m_trees.emplace_back(new TTree("signal_permutation","Signal tree"));
    m_trees.emplace_back(new TTree("background_permutation","Background tree"));

    for (auto& itree : m_trees) {
        itree->Branch("weight",       &m_weight);
        itree->Branch("jet1_pt",      &m_jet1_pt);
        itree->Branch("jet1_eta",     &m_jet1_eta);
        itree->Branch("jet1_phi",     &m_jet1_phi);
        itree->Branch("jet1_e",       &m_jet1_e);
        itree->Branch("jet1_tag_bin", &m_jet1_tag_bin);
        itree->Branch("jet2_pt",      &m_jet2_pt);
        itree->Branch("jet2_eta",     &m_jet2_eta);
        itree->Branch("jet2_phi",     &m_jet2_phi);
        itree->Branch("jet2_e",       &m_jet2_e);
        itree->Branch("jet2_tag_bin", &m_jet2_tag_bin);
        itree->Branch("jet3_pt",      &m_jet3_pt);
        itree->Branch("jet3_eta",     &m_jet3_eta);
        itree->Branch("jet3_phi",     &m_jet3_phi);
        itree->Branch("jet3_e",       &m_jet3_e);
        itree->Branch("jet3_tag_bin", &m_jet3_tag_bin);
        itree->Branch("jet4_pt",      &m_jet4_pt);
        itree->Branch("jet4_eta",     &m_jet4_eta);
        itree->Branch("jet4_phi",     &m_jet4_phi);
        itree->Branch("jet4_e",       &m_jet4_e);
        itree->Branch("jet4_tag_bin", &m_jet4_tag_bin);
        itree->Branch("jet_n",        &m_jet_n);
        itree->Branch("had_top_pt",   &m_had_top_pt);
        itree->Branch("had_top_eta",  &m_had_top_eta);
        itree->Branch("had_top_phi",  &m_had_top_phi);
        itree->Branch("had_top_m",    &m_had_top_m);
        itree->Branch("had_W_pt",     &m_had_W_pt);
        itree->Branch("had_W_eta",    &m_had_W_eta);
        itree->Branch("had_W_phi",    &m_had_W_phi);
        itree->Branch("had_W_m",      &m_had_W_m);
        itree->Branch("W_jets_dR",    &m_W_jets_dR);
        itree->Branch("b_lep_dR",     &m_b_lep_dR);
        itree->Branch("mlb",          &m_mlb);
        itree->Branch("pt_lb",        &m_pt_lb);
    }
}

void DnnTree::ProcessSingleEvent(const Event& event, const float& weight) {

    std::vector<int> indices = {0,1,2,3};
    if (event.jet_pt->size() > 4) {
        indices.emplace_back(4);
    }
    if (event.jet_pt->size() > 5) {
        indices.emplace_back(5);
    }

    TLorentzVector lep;
    if (event.ejets_2015 || event.ejets_2016 || event.ejets_2017 || event.ejets_2018) {
       lep.SetPtEtaPhiE(event.el_pt->at(0)/1e3,
                        event.el_eta->at(0),
                        event.el_phi->at(0),
                        event.el_e->at(0)/1e3);
    } else if (event.mujets_2015 || event.mujets_2016 || event.mujets_2017 || event.mujets_2018) {
       lep.SetPtEtaPhiE(event.mu_pt->at(0)/1e3,
                        event.mu_eta->at(0),
                        event.mu_phi->at(0),
                        event.mu_e->at(0)/1e3);
    } else {
        std::cerr << "DnnTree::ProcessSingleEvent: File is not el not mu" << std::endl;
        exit(EXIT_FAILURE);
    }

    std::vector<std::vector<std::vector<int> > > perm_type(4);

    m_weight = weight;
    m_jet_n = event.jet_pt->size();

    bool found_best(false);
    /// run the permutation loop
    while (std::next_permutation(indices.begin(), indices.end())) {
        /// we call a correct permutation when:
        // index 0 is bhad
        // index 1 is blep
        // index 2 is W up
        // index 3 is W down
        TLorentzVector bhad, blep, up, down;
        bhad.SetPtEtaPhiE(event.jet_pt->at(indices.at(0))/1e3,
                          event.jet_eta->at(indices.at(0)),
                          event.jet_phi->at(indices.at(0)),
                          event.jet_e->at(indices.at(0))/1e3);
        blep.SetPtEtaPhiE(event.jet_pt->at(indices.at(1))/1e3,
                          event.jet_eta->at(indices.at(1)),
                          event.jet_phi->at(indices.at(1)),
                          event.jet_e->at(indices.at(1))/1e3);
        up.SetPtEtaPhiE(event.jet_pt->at(indices.at(2))/1e3,
                        event.jet_eta->at(indices.at(2)),
                        event.jet_phi->at(indices.at(2)),
                        event.jet_e->at(indices.at(2))/1e3);
        down.SetPtEtaPhiE(event.jet_pt->at(indices.at(3))/1e3,
                          event.jet_eta->at(indices.at(3)),
                          event.jet_phi->at(indices.at(3)),
                          event.jet_e->at(indices.at(3))/1e3);

        m_had_top_pt  = (bhad + up + down).Pt();
        m_had_top_eta = (bhad + up + down).Eta();
        m_had_top_phi = (bhad + up + down).Phi();
        m_had_top_m   = (bhad + up + down).M();
        m_had_W_pt    = (up + down).Pt();
        m_had_W_eta   = (up + down).Eta();
        m_had_W_phi   = (up + down).Phi();
        m_had_W_m     = (up + down).M();
        m_W_jets_dR   = up.DeltaR(down);
        m_b_lep_dR    = lep.DeltaR(blep);
        m_mlb         = (lep + blep).M();
        m_pt_lb       = (lep + blep).Pt();

        m_jet1_pt = event.jet_pt->at(indices.at(0))/1e3;
        m_jet1_eta = event.jet_eta->at(indices.at(0));
        m_jet1_phi = event.jet_phi->at(indices.at(0));
        m_jet1_e = event.jet_e->at(indices.at(0))/1e3;
        m_jet1_tag_bin = event.jet_tagWeightBin_DL1r_Continuous->at(indices.at(0));
        m_jet2_pt = event.jet_pt->at(indices.at(1))/1e3;
        m_jet2_eta = event.jet_eta->at(indices.at(1));
        m_jet2_phi = event.jet_phi->at(indices.at(1));
        m_jet2_e = event.jet_e->at(indices.at(1)/1e3);
        m_jet2_tag_bin = event.jet_tagWeightBin_DL1r_Continuous->at(indices.at(1));
        m_jet3_pt = event.jet_pt->at(indices.at(2))/1e3;
        m_jet3_eta = event.jet_eta->at(indices.at(2));
        m_jet3_phi = event.jet_phi->at(indices.at(2));
        m_jet3_e = event.jet_e->at(indices.at(2))/1e3;
        m_jet3_tag_bin = event.jet_tagWeightBin_DL1r_Continuous->at(indices.at(2));
        m_jet4_pt = event.jet_pt->at(indices.at(3))/1e3;
        m_jet4_eta = event.jet_eta->at(indices.at(3));
        m_jet4_phi = event.jet_phi->at(indices.at(3));
        m_jet4_e = event.jet_e->at(indices.at(3))/1e3;
        m_jet4_tag_bin = event.jet_tagWeightBin_DL1r_Continuous->at(indices.at(3));

        const std::size_t correct = GetNumberOfMatched(indices, event);
        if (correct == 4) {
            if (!found_best) {
                found_best = true;
                /// all four jets identified
                m_trees[0]->Fill();
            }
        } else {
            bool is_present(false);
            for (const auto& ivec : perm_type.at(correct)) {
                if (ivec == indices) {
                    is_present = true;
                    break;
                }
            }

            if (!is_present) perm_type.at(correct).emplace_back(indices);
        }
    }

    /// now take random permutation from each category as bkg
    for (const auto& itype : perm_type) {
        if (itype.size() == 0) continue;
        const std::size_t element = m_random->Uniform(itype.size());

        TLorentzVector bhad, blep, up, down;
        bhad.SetPtEtaPhiE(event.jet_pt->at(indices.at(0))/1e3,
                          event.jet_eta->at(indices.at(0)),
                          event.jet_phi->at(indices.at(0)),
                          event.jet_e->at(indices.at(0))/1e3);
        blep.SetPtEtaPhiE(event.jet_pt->at(indices.at(1))/1e3,
                          event.jet_eta->at(indices.at(1)),
                          event.jet_phi->at(indices.at(1)),
                          event.jet_e->at(indices.at(1))/1e3);
        up.SetPtEtaPhiE(event.jet_pt->at(indices.at(2))/1e3,
                        event.jet_eta->at(indices.at(2)),
                        event.jet_phi->at(indices.at(2)),
                        event.jet_e->at(indices.at(2))/1e3);
        down.SetPtEtaPhiE(event.jet_pt->at(indices.at(3))/1e3,
                          event.jet_eta->at(indices.at(3)),
                          event.jet_phi->at(indices.at(3)),
                          event.jet_e->at(indices.at(3))/1e3);

        m_had_top_pt  = (bhad + up + down).Pt();
        m_had_top_eta = (bhad + up + down).Eta();
        m_had_top_phi = (bhad + up + down).Phi();
        m_had_top_m   = (bhad + up + down).M();
        m_had_W_pt    = (up + down).Pt();
        m_had_W_eta   = (up + down).Eta();
        m_had_W_phi   = (up + down).Phi();
        m_had_W_m     = (up + down).M();
        m_W_jets_dR   = up.DeltaR(down);
        m_b_lep_dR    = lep.DeltaR(blep);
        m_mlb         = (lep + blep).M();
        m_pt_lb       = (lep + blep).Pt();

        m_jet1_pt = event.jet_pt->at(itype.at(element).at(0))/1e3;
        m_jet1_eta = event.jet_eta->at(itype.at(element).at(0));
        m_jet1_phi = event.jet_phi->at(itype.at(element).at(0));
        m_jet1_e = event.jet_e->at(itype.at(element).at(0))/1e3;
        m_jet1_tag_bin = event.jet_tagWeightBin_DL1r_Continuous->at(indices.at(0));
        m_jet2_pt = event.jet_pt->at(itype.at(element).at(1))/1e3;
        m_jet2_eta = event.jet_eta->at(itype.at(element).at(1));
        m_jet2_phi = event.jet_phi->at(itype.at(element).at(1));
        m_jet2_e = event.jet_e->at(itype.at(element).at(1)/1e3);
        m_jet2_tag_bin = event.jet_tagWeightBin_DL1r_Continuous->at(indices.at(1));
        m_jet3_pt = event.jet_pt->at(itype.at(element).at(2))/1e3;
        m_jet3_eta = event.jet_eta->at(itype.at(element).at(2));
        m_jet3_phi = event.jet_phi->at(itype.at(element).at(2));
        m_jet3_e = event.jet_e->at(itype.at(element).at(2))/1e3;
        m_jet3_tag_bin = event.jet_tagWeightBin_DL1r_Continuous->at(indices.at(2));
        m_jet4_pt = event.jet_pt->at(itype.at(element).at(3))/1e3;
        m_jet4_eta = event.jet_eta->at(itype.at(element).at(3));
        m_jet4_phi = event.jet_phi->at(itype.at(element).at(3));
        m_jet4_e = event.jet_e->at(itype.at(element).at(3))/1e3;
        m_jet4_tag_bin = event.jet_tagWeightBin_DL1r_Continuous->at(indices.at(3));
        m_trees[1]->Fill();
    }
}

void DnnTree::Write() {
    m_file->cd();
    
    for (auto& itree : m_trees) {
        itree->Write();
    }
    
    for (auto& itree : m_trees) {
        delete itree;
    }

    m_file->Close();
}

std::size_t DnnTree::GetNumberOfMatched(const std::vector<int>& indices, const Event& event) const {
   std::size_t result(0);

   if (indices.at(0) == event.index_b_had) ++result;
   if (indices.at(1) == event.index_b_lep) ++result;
   if (indices.at(2) == event.index_W_up) ++result;
   if (indices.at(3) == event.index_W_down) ++result;

   return result; 
}
