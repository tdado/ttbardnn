#include "TtbarDNN/Event.h"

#include <iostream>

Event::Event(TTree *tree, bool isMC, bool isTT, bool isLoose, bool isSyst) :
    m_isMC(isMC),
    m_isTT(isTT),
    m_isLoose(isLoose),
    m_isSyst(isSyst),
    fChain(nullptr) {

    if (!tree) {
        std::cerr << "Event::Event: Tree is nullptr" << std::endl;
    } else {
        Init(tree);
    }
}

Event::~Event() {
}

Int_t Event::GetEntry(Long64_t entry) {
// Read contents of entry.
    if (!fChain) return 0;
    return fChain->GetEntry(entry);
}

Long64_t Event::LoadTree(Long64_t entry) {
// Set the environment to read one entry
    if (!fChain) return -5;
    Long64_t centry = fChain->LoadTree(entry);
    if (centry < 0) return centry;
    if (fChain->GetTreeNumber() != fCurrent) {
       fCurrent = fChain->GetTreeNumber();
    }
    return centry;
}

void Event::Init(TTree *tree) {
    // The Init() function is called when the selector needs to initialize
    // a new tree or chain. Typically here the branch addresses and branch
    // pointers of the tree will be set.
    // It is normally not necessary to make changes to the generated
    // code, but the routine can be extended by the user if needed.
    // Init() will be called many times when running on PROOF
    // (once per file to be processed).
    
    // Set object pointer
    mc_generator_weights = 0;
    weight_bTagSF_DL1r_Continuous_eigenvars_B_up = 0;
    weight_bTagSF_DL1r_Continuous_eigenvars_C_up = 0;
    weight_bTagSF_DL1r_Continuous_eigenvars_Light_up = 0;
    weight_bTagSF_DL1r_Continuous_eigenvars_B_down = 0;
    weight_bTagSF_DL1r_Continuous_eigenvars_C_down = 0;
    weight_bTagSF_DL1r_Continuous_eigenvars_Light_down = 0;
    el_pt = 0;
    el_eta = 0;
    el_cl_eta = 0;
    el_phi = 0;
    el_e = 0;
    el_charge = 0;
    el_CF = 0;
    mu_pt = 0;
    mu_eta = 0;
    mu_phi = 0;
    mu_e = 0;
    mu_charge = 0;
    el_isTight = 0;
    mu_isTight = 0;
    jet_pt = 0;
    jet_eta = 0;
    jet_phi = 0;
    jet_e = 0;
    jet_jvt = 0;
    jet_tagWeightBin_DL1r_Continuous = 0;

    // Set branch addresses and branch pointers
    if (!tree) return;
    fChain = tree;
    fCurrent = -1;
    fChain->SetMakeClass(1);
    
    if (m_isMC) {
        fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
        fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
        fChain->SetBranchAddress("weight_leptonSF", &weight_leptonSF, &b_weight_leptonSF);
        fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous", &weight_bTagSF_DL1r_Continuous, &b_weight_bTagSF_DL1r_Continuous);
        fChain->SetBranchAddress("weight_jvt", &weight_jvt, &b_weight_jvt);
        if (!m_isSyst){
            fChain->SetBranchAddress("mc_generator_weights", &mc_generator_weights, &b_mc_generator_weights);
            fChain->SetBranchAddress("weight_pileup_UP", &weight_pileup_UP, &b_weight_pileup_UP);
            fChain->SetBranchAddress("weight_pileup_DOWN", &weight_pileup_DOWN, &b_weight_pileup_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_UP", &weight_leptonSF_EL_SF_Trigger_UP, &b_weight_leptonSF_EL_SF_Trigger_UP);
            fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_DOWN", &weight_leptonSF_EL_SF_Trigger_DOWN, &b_weight_leptonSF_EL_SF_Trigger_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_UP", &weight_leptonSF_EL_SF_Reco_UP, &b_weight_leptonSF_EL_SF_Reco_UP);
            fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_DOWN", &weight_leptonSF_EL_SF_Reco_DOWN, &b_weight_leptonSF_EL_SF_Reco_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_UP", &weight_leptonSF_EL_SF_ID_UP, &b_weight_leptonSF_EL_SF_ID_UP);
            fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_DOWN", &weight_leptonSF_EL_SF_ID_DOWN, &b_weight_leptonSF_EL_SF_ID_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_UP", &weight_leptonSF_EL_SF_Isol_UP, &b_weight_leptonSF_EL_SF_Isol_UP);
            fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_DOWN", &weight_leptonSF_EL_SF_Isol_DOWN, &b_weight_leptonSF_EL_SF_Isol_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_UP", &weight_leptonSF_MU_SF_Trigger_STAT_UP, &b_weight_leptonSF_MU_SF_Trigger_STAT_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_DOWN", &weight_leptonSF_MU_SF_Trigger_STAT_DOWN, &b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_UP", &weight_leptonSF_MU_SF_Trigger_SYST_UP, &b_weight_leptonSF_MU_SF_Trigger_SYST_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_DOWN", &weight_leptonSF_MU_SF_Trigger_SYST_DOWN, &b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_UP", &weight_leptonSF_MU_SF_ID_STAT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_UP", &weight_leptonSF_MU_SF_ID_SYST_UP, &b_weight_leptonSF_MU_SF_ID_SYST_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_DOWN", &weight_leptonSF_MU_SF_ID_SYST_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_UP", &weight_leptonSF_MU_SF_Isol_STAT_UP, &b_weight_leptonSF_MU_SF_Isol_STAT_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_DOWN", &weight_leptonSF_MU_SF_Isol_STAT_DOWN, &b_weight_leptonSF_MU_SF_Isol_STAT_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_UP", &weight_leptonSF_MU_SF_Isol_SYST_UP, &b_weight_leptonSF_MU_SF_Isol_SYST_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_DOWN", &weight_leptonSF_MU_SF_Isol_SYST_DOWN, &b_weight_leptonSF_MU_SF_Isol_SYST_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_UP", &weight_leptonSF_MU_SF_TTVA_STAT_UP, &b_weight_leptonSF_MU_SF_TTVA_STAT_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_DOWN", &weight_leptonSF_MU_SF_TTVA_STAT_DOWN, &b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_UP", &weight_leptonSF_MU_SF_TTVA_SYST_UP, &b_weight_leptonSF_MU_SF_TTVA_SYST_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_DOWN", &weight_leptonSF_MU_SF_TTVA_SYST_DOWN, &b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN);
            fChain->SetBranchAddress("weight_jvt_UP", &weight_jvt_UP, &b_weight_jvt_UP);
            fChain->SetBranchAddress("weight_jvt_DOWN", &weight_jvt_DOWN, &b_weight_jvt_DOWN);
            fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_B_up", &weight_bTagSF_DL1r_Continuous_eigenvars_B_up, &b_weight_bTagSF_DL1r_Continuous_eigenvars_B_up);
            fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_C_up", &weight_bTagSF_DL1r_Continuous_eigenvars_C_up, &b_weight_bTagSF_DL1r_Continuous_eigenvars_C_up);
            fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_Light_up", &weight_bTagSF_DL1r_Continuous_eigenvars_Light_up, &b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_up);
            fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_B_down", &weight_bTagSF_DL1r_Continuous_eigenvars_B_down, &b_weight_bTagSF_DL1r_Continuous_eigenvars_B_down);
            fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_C_down", &weight_bTagSF_DL1r_Continuous_eigenvars_C_down, &b_weight_bTagSF_DL1r_Continuous_eigenvars_C_down);
            fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_Light_down", &weight_bTagSF_DL1r_Continuous_eigenvars_Light_down, &b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_down);
        }
    }
    fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
    fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
    fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
    fChain->SetBranchAddress("mu", &mu, &b_mu);
    fChain->SetBranchAddress("mu_actual", &mu_actual, &b_mu_actual);
    fChain->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
    fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
    fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
    fChain->SetBranchAddress("el_cl_eta", &el_cl_eta, &b_el_cl_eta);
    fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
    fChain->SetBranchAddress("el_e", &el_e, &b_el_e);
    fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
    fChain->SetBranchAddress("el_CF", &el_CF, &b_el_CF);
    fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
    fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
    fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
    fChain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
    fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
    if (m_isLoose) {
        fChain->SetBranchAddress("el_isTight", &el_isTight, &b_el_isTight);
        fChain->SetBranchAddress("mu_isTight", &mu_isTight, &b_mu_isTight);
    }
    fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
    fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
    fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
    fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
    fChain->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
    fChain->SetBranchAddress("jet_tagWeightBin_DL1r_Continuous", &jet_tagWeightBin_DL1r_Continuous, &b_jet_tagWeightBin_DL1r_Continuous);
    fChain->SetBranchAddress("met_met", &met_met, &b_met_met);
    fChain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
    fChain->SetBranchAddress("ejets_2015", &ejets_2015, &b_ejets_2015);
    fChain->SetBranchAddress("ejets_2016", &ejets_2016, &b_ejets_2016);
    fChain->SetBranchAddress("ejets_2017", &ejets_2017, &b_ejets_2017);
    fChain->SetBranchAddress("ejets_2018", &ejets_2018, &b_ejets_2018);
    fChain->SetBranchAddress("mujets_2015", &mujets_2015, &b_mujets_2015);
    fChain->SetBranchAddress("mujets_2016", &mujets_2016, &b_mujets_2016);
    fChain->SetBranchAddress("mujets_2017", &mujets_2017, &b_mujets_2017);
    fChain->SetBranchAddress("mujets_2018", &mujets_2018, &b_mujets_2018);
    if (m_isMC) {
        fChain->SetBranchAddress("is_AFII", &is_AFII, &b_is_AFII);
        if (m_isTT) {
            fChain->SetBranchAddress("index_W_up", &index_W_up, &b_index_W_up);
            fChain->SetBranchAddress("index_W_down", &index_W_down, &b_index_W_down);
            fChain->SetBranchAddress("index_b_lep", &index_b_lep, &b_index_b_lep);
            fChain->SetBranchAddress("index_b_had", &index_b_had, &b_index_b_had);
            fChain->SetBranchAddress("isDilepton", &isDilepton, &b_isDilepton);
            fChain->SetBranchAddress("W_quark_up_pt", &W_quark_up_pt, &b_W_quark_up_pt);
            fChain->SetBranchAddress("W_quark_up_eta", &W_quark_up_eta, &b_W_quark_up_eta);
            fChain->SetBranchAddress("W_quark_up_phi", &W_quark_up_phi, &b_W_quark_up_phi);
            fChain->SetBranchAddress("W_quark_up_m", &W_quark_up_m, &b_W_quark_up_m);
            fChain->SetBranchAddress("W_quark_down_pt", &W_quark_down_pt, &b_W_quark_down_pt);
            fChain->SetBranchAddress("W_quark_down_eta", &W_quark_down_eta, &b_W_quark_down_eta);
            fChain->SetBranchAddress("W_quark_down_phi", &W_quark_down_phi, &b_W_quark_down_phi);
            fChain->SetBranchAddress("W_quark_down_m", &W_quark_down_m, &b_W_quark_down_m);
            fChain->SetBranchAddress("b_quark_lep_pt", &b_quark_lep_pt, &b_b_quark_lep_pt);
            fChain->SetBranchAddress("b_quark_lep_eta", &b_quark_lep_eta, &b_b_quark_lep_eta);
            fChain->SetBranchAddress("b_quark_lep_phi", &b_quark_lep_phi, &b_b_quark_lep_phi);
            fChain->SetBranchAddress("b_quark_lep_m", &b_quark_lep_m, &b_b_quark_lep_m);
            fChain->SetBranchAddress("b_quark_had_pt", &b_quark_had_pt, &b_b_quark_had_pt);
            fChain->SetBranchAddress("b_quark_had_eta", &b_quark_had_eta, &b_b_quark_had_eta);
            fChain->SetBranchAddress("b_quark_had_phi", &b_quark_had_phi, &b_b_quark_had_phi);
            fChain->SetBranchAddress("b_quark_had_m", &b_quark_had_m, &b_b_quark_had_m);
            fChain->SetBranchAddress("lepton_pt", &lepton_pt, &b_lepton_pt);
            fChain->SetBranchAddress("lepton_eta", &lepton_eta, &b_lepton_eta);
            fChain->SetBranchAddress("lepton_phi", &lepton_phi, &b_lepton_phi);
            fChain->SetBranchAddress("lepton_m", &lepton_m, &b_lepton_m);
            fChain->SetBranchAddress("neutrino_pt", &neutrino_pt, &b_neutrino_pt);
            fChain->SetBranchAddress("neutrino_eta", &neutrino_eta, &b_neutrino_eta);
            fChain->SetBranchAddress("neutrino_phi", &neutrino_phi, &b_neutrino_phi);
            fChain->SetBranchAddress("neutrino_m", &neutrino_m, &b_neutrino_m);
        }
    }
}
