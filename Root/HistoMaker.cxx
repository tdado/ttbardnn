#include "TtbarDNN/HistoMaker.h"
#include "TtbarDNN/Event.h"
#include "TtbarDNN/WeightManager.h"

#include "TDirectory.h"
#include "TFile.h"
#include "TTree.h"

#include <algorithm>
#include <iostream>

HistoMaker::HistoMaker(const std::string& type,
                       const std::string& systematics,
                       const std::vector<std::string>& sfsyst_list) :
    m_type(type),
    m_systematics(systematics),
    m_current_syst("nominal"),
    m_isMC(true),
    m_isTT(false),
    m_isPP8(false),
    m_isNominal(false),
    m_tree_name("nominal"),
    m_sfsyst_list(sfsyst_list) {
    
    /// Check if the file is ttbar
    static const std::vector<std::string> ttbar_processes = {"ttbar_PP8_FS",
                                                             "ttbar_PP8_AFII",
                                                             "ttbar_PH7_0_4_AFII",
                                                             "ttbar_PH7_1_3_AFII",
                                                             "ttbar_aMCNLO_Pythia8_AFII",
                                                             "ttbar_aMCNLO_Herwig7_1_3_AFII"};
    
    if (std::find(ttbar_processes.begin(), ttbar_processes.end(), m_type) != ttbar_processes.end()){
        m_isTT = true;
    }

    if (m_type == "ttbar_PP8_FS" || m_type == "ttbar_PP8_FS") {
        m_isPP8 = true;
    }
    
    if (m_systematics == "nominal") {
        m_isNominal = true;
    }
    

    /// Allocating on stack as these are huge objects
    m_control_plots = std::unique_ptr<ControlPlots> (new ControlPlots());

    m_control_plots->Init(sfsyst_list);


}

void HistoMaker::FillHistos(const WeightManager& wei_manager,
                            const std::string& tree_name,
                            const std::string& path) {

    
    /// Set the tree name
    m_tree_name = tree_name;

    std::unique_ptr<TFile> file(new TFile(path.c_str(), "READ"));
    if (!file) {
        std::cerr << "HistoMaker::FillHistos: Cannot open input file: " << path << ", skipping" << std::endl;
        return;
    }
    TTree *tree = (static_cast<TTree*>(file->Get(tree_name.c_str())));
    if (!tree) {
        std::cerr << "HistoMaker::FillHistos: Cannot read tree: " << tree_name << " from file: " << path <<", skipping" << std::endl;
        return;
    }

    const int nentries = tree->GetEntries();

    bool use_loose(false);
    bool is_syst(false);
    if (m_type == "Multijet") use_loose = true;
    if (m_systematics == "syst") is_syst = true;

    Event event(tree, m_isMC, m_isTT, use_loose, is_syst);

    /// Run event loop
    for (int ievent = 0; ievent < nentries; ++ievent) {
        event.GetEntry(ievent);
        if (ievent % 100000 == 0) {
            std::cout << "\t\tProcessing event " << ievent << ", out of " << nentries << " events\n";
        }
        
        if (event.ejets_2015 || event.ejets_2016 || event.ejets_2017 || event.ejets_2018) m_lepton_type = Common::LEPTONTYPE::EL;
        else if (event.mujets_2015 || event.mujets_2016 || event.mujets_2017 || event.mujets_2018) m_lepton_type = Common::LEPTONTYPE::MU;


        /// Prepare vector with weights
        std::vector<double> wei_vec;
        if (m_isMC) {
            if (m_systematics == "sfsyst") {
                wei_vec = wei_manager.GetSfSystWeights(event);
            } else {
                wei_vec.emplace_back(wei_manager.GetNominalWeight(event));
            }

            if (wei_vec.size() != m_sfsyst_list.size()) {
                std::cerr << "HistoMaker::FillHistos: Size of sf names and sf weights do not match" << std::endl;
                std::cerr << "HistoMaker::FillHistos: Size of sf names: " << m_sfsyst_list.size() << ", size of weights: " << wei_vec.size() << std::endl;
                exit(EXIT_FAILURE);
            }
        } else {
            // weight is one for data
            wei_vec.emplace_back(1.);
        }
        m_control_plots->FillAllHistos(event, 2, 3, m_isMC, wei_vec); // Passing some random values now
        if (m_systematics == "nominal" && m_isTT) {
        }
    }

    delete tree;
    file->Close();
}

void HistoMaker::Finalise() {
    m_control_plots->Finalise();
    if (m_systematics == "nominal" && m_isTT) {
    }
}

void HistoMaker::WriteHistosToFile(const std::string& path, const std::string& name) {
    const std::string suffix = m_isNominal ? ".temp" : ""; 
    const std::string final_path = path+"/"+name+"/"+m_type+"_"+name+".root"+suffix;

    std::unique_ptr<TFile> file(new TFile(final_path.c_str(), "UPDATE"));
    if (!file) {
        std::cerr << "HistoMaker::WriteHistosToFile: Cannot open file at: " << final_path << std::endl;
        exit(EXIT_FAILURE);
    }
    
    /// Create the directory structure
    if (m_isNominal) {
        file->cd();
        gDirectory->mkdir("nominal");
    } else if (m_systematics == "syst") {
        if (!file->Get(m_current_syst.c_str())) {
            file->cd();
            gDirectory->mkdir(m_current_syst.c_str());
        }
    } else if (m_systematics == "sfsyst") {
        for (const auto& isf : m_sfsyst_list) {
            if (!file->Get(isf.c_str())) {
                file->cd();
                gDirectory->mkdir(isf.c_str());
            }
        }
    }

    m_control_plots->Write(m_current_syst, file.get());

    file->Close();
}
    
