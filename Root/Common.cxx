#include "TtbarDNN/Common.h"

#include "TH1D.h"

#include <algorithm>

void Common::AddOverflow(TH1D* hist) {
    const int nbins = hist->GetNbinsX();

    hist->SetBinContent(1, hist->GetBinContent(0) + hist->GetBinContent(1) );
    hist->SetBinContent(nbins, hist->GetBinContent(nbins) + hist->GetBinContent(nbins+1) );
    hist->SetBinContent(0, 0 );
    hist->SetBinContent(nbins+1, 0 );
    
    hist->SetBinError(1, std::sqrt(hist->GetBinError(0)*hist->GetBinError(0) + hist->GetBinError(1)*hist->GetBinError(1)));
    hist->SetBinError(nbins, std::sqrt(hist->GetBinError(nbins)*hist->GetBinError(nbins) + hist->GetBinError(nbins+1)*hist->GetBinError(nbins+1)));
    hist->SetBinError(0, 0);
    hist->SetBinError(nbins+1, 0);
}

std::string Common::RemoveSubstrings(const std::string& str, const std::vector<std::string>& remove) {
    std::string result = str;
    result.erase(std::remove(result.begin(), result.end(), '_'), result.end());

    for (const auto& i : remove) {
        auto itr = result.find(i.c_str());       
        if (itr == std::string::npos) continue;

        const auto size = i.size();
        result.erase(itr, size);
    }

    return result;
}

float Common::GetMax(TH1D* h1, TH1D* h2) {
    const float max1 = h1->GetMaximum();
    const float max2 = h2->GetMaximum();

    return std::max(max1, max2);
}
