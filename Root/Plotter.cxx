#include "TtbarDNN/Plotter.h"
#include "TtbarDNN/Common.h"
#include "AtlasUtils/AtlasStyle.h"

#include "TCanvas.h"
#include "TColor.h"
#include "TGraphAsymmErrors.h"
#include "TH1D.h"
#include "TLatex.h"
#include "TLine.h"
#include "TLegend.h"
#include "TPad.h"
#include "TSystem.h"

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <fstream>

Plotter::Plotter(const std::string& directory) :
    m_directory(directory),
    m_data_name("SetMe"),
    m_data_file(nullptr),
    m_atlas_label("Internal"),
    m_lumi_label("139"),
    m_collection(""),
    m_nominal_ttbar_name(""),
    m_nominal_ttbar_index(999),
    m_syst_shape_only(false),
    m_run_syst(false) {

    SetAtlasStyle();
    FillStyleMap();
    SetColourMap();
    SetLabelMap();
}

void Plotter::SetTTbarNames(const std::vector<std::string>& names) {
    if (names.size() == 0) {
        std::cerr << "Plotter::SetTTbarNames: Passed name vector is empty!" << std::endl;
        exit(EXIT_FAILURE);
    }
    m_ttbar_names = names;
}

void Plotter::SetBackgroundNames(const std::vector<std::string>& names) {
    if (names.size() == 0) {
        std::cerr << "Plotter::SetBackgroundNames: Passed name vector is empty!" << std::endl;
        exit(EXIT_FAILURE);
    }
    m_background_names = names;
}

void Plotter::SetSpecialNames(const std::vector<std::string>& names) {
    if (names.size() == 0) {
        std::cerr << "Plotter::SetSpecialNames: Passed name vector is empty!" << std::endl;
        exit(EXIT_FAILURE);
    }
    m_special_names = names;
}

void Plotter::SetDataName(const std::string& name) {
    m_data_name = name;
}

void Plotter::SetAtlasLabel(const std::string& label) {
    m_atlas_label = label;
}

void Plotter::SetLumiLabel(const std::string& label) {
    m_lumi_label = label;
}

void Plotter::SetCollection(const std::string& collection) {
    m_collection = collection;
}

void Plotter::SetNominalTTbarName(const std::string& name) {
    m_nominal_ttbar_name = name;

    auto itr = std::find(m_ttbar_names.begin(), m_ttbar_names.end(), name);
    if (itr == m_ttbar_names.end()) {
        std::cerr << "Plotter::SetNominalTTbarName: The nominal ttbar name is not in the list of ttbar names" << std::endl;
        exit(EXIT_FAILURE);
    }

    m_nominal_ttbar_index = std::distance(m_ttbar_names.begin(), itr);
}

void Plotter::OpenRootFiles() {

    /// TTbar files
    for (const auto& ittbar : m_ttbar_names) {
        std::unique_ptr<TFile> f(new TFile(("../OutputHistos/"+m_directory+"/"+ittbar+"_"+m_directory+".root").c_str(), "READ"));
        if (!f) {
            std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/"+m_directory+"/"+ittbar+"_"+m_directory+".root" << std::endl;
            exit(EXIT_FAILURE); 
        }

        m_ttbar_files.emplace_back(std::move(f));
    }
    
    /// Background files
    for (const auto& ibkg : m_background_names) {
        std::unique_ptr<TFile> f(new TFile(("../OutputHistos/"+m_directory+"/"+ibkg+"_"+m_directory+".root").c_str(), "READ"));
        if (!f) {
            std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/"+m_directory+"/"+ibkg+"_"+m_directory+".root" << std::endl;
            exit(EXIT_FAILURE); 
        }

        m_background_files.emplace_back(std::move(f));
    }

    /// Special files
    for (const auto& ispecial : m_special_names) {
        std::unique_ptr<TFile> f(new TFile(("../OutputHistos/"+m_directory+"/"+ispecial+"_"+m_directory+".root").c_str(), "READ"));
        if (!f) {
            std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/"+m_directory+"/"+ispecial+"_"+m_directory+".root" << std::endl;
            exit(EXIT_FAILURE);
        }

        m_special_files.emplace_back(std::move(f));
    }

    /// Data File
    m_data_file = std::unique_ptr<TFile>(new TFile(("../OutputHistos/"+m_directory+"/"+m_data_name+"_"+m_directory+".root").c_str(), "READ"));
    if (!m_data_file) {
        std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/"+m_directory+"/"+m_data_name+"_"+m_directory+".root" << std::endl;
        exit(EXIT_FAILURE); 
    }

    if ((m_ttbar_files.size() == 0) || (m_background_files.size() == 0)) {
        std::cerr << "Plotter::OpenRootFiles: Sizes of ttbar or background files are empty" << std::endl;
        exit(EXIT_FAILURE);
    }
    
    /// Create directory
    gSystem->mkdir(("../Plots/"+m_directory).c_str());
}

void Plotter::PlotDataMCPlots(const std::string& name,
                              const std::string& axis,
                              const std::string& elements,
                              const std::string& units,
                              const bool& logY) const {
    std::cout << "Plotter::PlotDataMCPlots: Plotting data/MC plot: " << name << "\n";

    std::unique_ptr<TH1D> ttbar_histo(static_cast<TH1D*>(m_ttbar_files.at(m_nominal_ttbar_index)->Get(("nominal/"+name).c_str())));
    if (!ttbar_histo) {
        std::cerr << "Plotter::PlotDataMCPlots: Cannot read ttbar histo: " << name << ", skipping\n";
        return; 
    }

    std::vector<std::unique_ptr<TH1D> > background_histos;
    for (const auto& ibkg : m_background_files) {
        TH1D* h = static_cast<TH1D*>(ibkg->Get(("nominal/"+name).c_str()));
        if (!h) {
            std::cerr << "Plotter::PlotDataMCPlots: Cannot read background histos: " << name << ", skipping\n";
            return;
        }

        background_histos.emplace_back(std::move(h));
    }

    std::unique_ptr<TH1D> data_histo(static_cast<TH1D*>(m_data_file->Get(("nominal/"+name).c_str())));
    if (!data_histo) {
        std::cerr << "Plotter::PlotDataMCPlots: Cannot read data histogram: " << name << ", skipping\n";
        return;
    }

    /// Stack background histograms
    for (std::size_t ibkg = 0; ibkg < background_histos.size(); ++ibkg) {
        for (std::size_t i = ibkg + 1; i < background_histos.size(); ++i) {
            background_histos.at(ibkg)->Add(background_histos.at(i).get());
        }
    }

    /// Stack signal as well
    ttbar_histo->Add(background_histos.at(0).get());

    if (m_syst_shape_only) {
        if (data_histo->Integral() > 1e-6 && ttbar_histo->Integral() > 1e-6) {
            ttbar_histo->Scale(data_histo->Integral()/ttbar_histo->Integral());
        }
    }

    /// Cosmetics
    ttbar_histo->SetFillColor(0);
    ttbar_histo->SetMarkerStyle(21);
    ttbar_histo->SetLineColor(1);
    ttbar_histo->SetLineWidth(2);
    if (logY) {
        ttbar_histo->SetMinimum(1.001);
        ttbar_histo->SetMaximum(1e4*ttbar_histo->GetMaximum());
    } else {
        ttbar_histo->SetMinimum(0.001);
        ttbar_histo->SetMaximum(1.8*ttbar_histo->GetMaximum());
    }

    const float bin_width = ttbar_histo->GetBinWidth(1);
    if (bin_width < 1) {
        if (m_syst_shape_only) {
            ttbar_histo->GetYaxis()->SetTitle(Form("Normalised to data / %.2f %s", bin_width, units.c_str()));
        } else {
            ttbar_histo->GetYaxis()->SetTitle(Form("%s / %.2f %s", elements.c_str(), bin_width, units.c_str()));
        }
    } else {
        if (m_syst_shape_only) {
            ttbar_histo->GetYaxis()->SetTitle(Form("Normalised to data / %.f %s", bin_width, units.c_str()));
        } else {
            ttbar_histo->GetYaxis()->SetTitle(Form("%s / %.f %s", elements.c_str(), bin_width, units.c_str()));
        }
    }

    for (std::size_t ibkg = 0; ibkg < m_background_names.size(); ++ibkg) {
        auto itr = m_colour_map.find(m_background_names.at(ibkg));
        if (itr == m_colour_map.end()) {
            std::cerr << "Plotter::PlotDataMCPlots: Cannot find " << m_background_names.at(ibkg) << ", in the colour map\n";
            exit(EXIT_FAILURE);
        }
        
        background_histos.at(ibkg)->SetFillColor(itr->second);
        background_histos.at(ibkg)->SetMarkerStyle(21);
        background_histos.at(ibkg)->SetLineColor(itr->second);
        background_histos.at(ibkg)->SetLineWidth(2);
    }

    /// Canvas
    TCanvas c("","",800,600);
    TPad pad1("pad1","pad1",0.0, 0.3, 1.0, 1.00);
    TPad pad2("pad2","pad2", 0.0, 0.010, 1.0, 0.3);
    pad1.SetBottomMargin(0.001);
    pad1.SetBorderMode(0);
    pad2.SetBottomMargin(0.5);
    pad1.SetTicks(1,1);
    pad2.SetTicks(1,1);
    pad1.Draw();
    pad2.Draw();
    
    /// Remove MC stat uncertainties 
    for (int ibin = 1; ibin <= ttbar_histo->GetNbinsX(); ++ibin) {
        ttbar_histo->SetBinError(ibin, 0);
    }

    std::unique_ptr<TH1D> total_up  (static_cast<TH1D*>(ttbar_histo->Clone()));
    std::unique_ptr<TH1D> total_down(static_cast<TH1D*>(ttbar_histo->Clone()));

    if (m_run_syst) {
        GetTotalUpDownUncertainty(total_up.get(), total_down.get(), name);
    }

    TGraphAsymmErrors error(ttbar_histo.get());

    if (m_run_syst) {
        TransformErrorHistoToTGraph(&error, total_up.get(), total_down.get());
    }
    
    error.SetFillStyle(3002);
    error.SetFillColor(kBlack);
    error.SetMarkerStyle(0);
    error.SetLineWidth(2);

    DrawUpperDataMCPlot(&pad1, ttbar_histo.get(), background_histos, data_histo.get(), &error);
    
    pad1.RedrawAxis();

    /// Draw legend
    static const std::vector<std::string> exclude = {"Pythia8", "aMCNLO", "Herwig", "Sherpa", "FS", "AFII", "PP8"};
    TLegend leg(0.70, 0.5, 0.85, 0.9);
    leg.AddEntry(data_histo.get(), "Data", "ep");
    leg.AddEntry(ttbar_histo.get(), "t#bar{t}", "f");
    for (std::size_t ibkg = 0; ibkg < m_background_names.size(); ++ibkg) {
        if (m_background_names.at(ibkg) == "ttH_PP8_FS") continue;
        auto label_itr = m_label_map.find(m_background_names.at(ibkg));
        if (label_itr == m_label_map.end()) {
            leg.AddEntry(background_histos.at(ibkg).get(), m_background_names.at(ibkg).c_str(), "f");
        } else {
            leg.AddEntry(background_histos.at(ibkg).get(), label_itr->second.c_str(), "f");
        }
    }
    if (m_run_syst) {
        if (m_syst_shape_only) {
            leg.AddEntry(&error, "Shape unc.", "f");
        } else {
            leg.AddEntry(&error, "Uncertainty", "f");
        }
    }

    leg.SetFillColor(0);
    leg.SetLineColor(0);
    leg.SetBorderSize(0);
    leg.SetTextFont(72);
    leg.SetTextSize(0.045);
    leg.Draw("same");

    std::unique_ptr<TH1D> combined_ratio (static_cast<TH1D*>(ttbar_histo->Clone()));
    std::unique_ptr<TH1D> data_ratio (static_cast<TH1D*>(data_histo->Clone()));

    TGraphAsymmErrors error_ratio(combined_ratio.get());
    for (int ibin = 0; ibin < error_ratio.GetN(); ++ibin) {
        double x,y;
        error_ratio.GetPoint(ibin, x, y);
        error_ratio.SetPoint(ibin, x, 1.);
    }

    std::unique_ptr<TH1D> error_up(static_cast<TH1D*>(total_up->Clone()));
    std::unique_ptr<TH1D> error_down(static_cast<TH1D*>(total_down->Clone()));
    error_up->Divide(combined_ratio.get());
    error_down->Divide(combined_ratio.get());
    
    error_ratio.SetFillStyle(3002);
    error_ratio.SetFillColor(kBlack);
    error_ratio.SetMarkerStyle(0);
    error_ratio.SetLineWidth(2);

    if (m_run_syst) {
        TransformErrorHistoToTGraph(&error_ratio, error_up.get(), error_down.get());
    }

    DrawRatioDataMCPlot(&pad2, combined_ratio.get(), data_ratio.get(), axis, &error_ratio);

    /// Draw line
    const float min = data_ratio->GetXaxis()->GetBinLowEdge(1);
    const float max = data_ratio->GetXaxis()->GetBinUpEdge(data_ratio->GetNbinsX());
    TLine line (min, 1, max, 1);
    line.SetLineColor(kRed);
    line.SetLineStyle(2);
    line.SetLineWidth(3);
    line.Draw("same");

    std::string log_label("");
    if (logY) {
        pad1.SetLogy();
        log_label = "_log";
    }

    c.Print(("../Plots/"+m_directory+"/"+name+log_label+".png").c_str());
    c.Print(("../Plots/"+m_directory+"/"+name+log_label+".pdf").c_str());
    c.Print(("../Plots/"+m_directory+"/"+name+log_label+".eps").c_str());
}
    
void Plotter::CloseRootFiles() {
    for (auto& ifile : m_ttbar_files) {
        ifile->Close();
    }
    for (auto& ifile : m_background_files) {
        ifile->Close();
    }
    for (auto& ifile : m_special_files) {
        ifile->Close();
    }
    m_data_file->Close();
}

void Plotter::CalculateYields() const {
    std::cout << "Plotter::CalculateYields: Calculating event yields\n";
    
    std::unique_ptr<std::ofstream> html(new std::ofstream());
    html->open(("../Plots/"+m_directory+"/EventYields.html").c_str(), std::ios::trunc);
    if (!html->is_open() || !html->good()) {
        std::cerr << "Plotter::CalculateYields: Cannot open html page at: " << "../Plots/"+m_directory+"/EventYields.html" << std::endl;
       exit(EXIT_FAILURE); 
    }
    *html << "<html><head><title>EventYields</title></head><body>" 
    			<< "<table border = 1> <tr>"
    			<< "</tr>\n";
    *html << "<td>";
    *html << "<table border style=\"float&#58;left;margin-right:7cm;margin-left:5cm;\">\n";
    *html << "<tr>  ";
    *html << "<td> Type <td> EventYields\n";

    float total(0);

    /// Read the event yields
    std::unique_ptr<TH1D> ttbar(static_cast<TH1D*>(m_ttbar_files.at(m_nominal_ttbar_index)->Get("nominal/jet_n")));
    if (!ttbar) {
        std::cerr << "Plotter::CalculateYields: Cannot read ttbar histo!" << std::endl;
        exit(EXIT_FAILURE);
    }
    total+= ttbar->Integral();
    *html << "<tr> <td> " << m_ttbar_names.at(m_nominal_ttbar_index) << " <td> " << std::fixed << std::setprecision(1) << ttbar->Integral() << "\n"; 

    /// Backgrounds
    for (std::size_t ibkg = 0; ibkg < m_background_files.size(); ++ibkg) {
        std::unique_ptr<TH1D> h(static_cast<TH1D*>(m_background_files.at(ibkg)->Get("nominal/jet_n")));
        if (!h) {
            std::cerr << "Plotter::CalculateYields: Cannot read bkg histos file!" << std::endl;
            exit(EXIT_FAILURE);
        }
        total+= h->Integral();
        *html << "<tr> <td> " << m_background_names.at(ibkg) << " <td> " << std::fixed << std::setprecision(1) << h->Integral() << "\n"; 
    }

    /// Total
    *html << "<tr bgcolor=lightblue> <td> Total pred. <td> " << std::fixed << std::setprecision(1) << total << "\n"; 

    /// Data
    std::unique_ptr<TH1D> data(static_cast<TH1D*>(m_data_file->Get("nominal/jet_n")));
    if (!data) {
        std::cerr << "Plotter::CalculateYields: Cannot read data histo!" << std::endl;
        exit(EXIT_FAILURE);
    }
    *html << "<tr bgcolor=pink> <td> Data <td> " << std::fixed << std::setprecision(1) << data->Integral() << "\n"; 
    *html << "<tr bgcolor=#66b3ff> <td> " << "Diff.[%] " << " <td> " << std::fixed << std::setprecision(2) << 100*total/data->Integral() << "\n"; 

    *html << "</tr>\n";
    *html << "</table>\n";
    *html << "</td>\n";
    html->close();
}

void Plotter::PlotUpperComparison(const std::vector<std::unique_ptr<TH1D>>& histos,
                                  TPad* pad,
                                  const std::string& axis) const {

    if (histos.size() == 0) return;

    pad->cd();

    const float resize = MaxResize(histos, false);

    histos.at(0)->GetYaxis()->SetRangeUser(0.00001, resize);
    histos.at(0)->GetXaxis()->SetTitle(axis.c_str());

    for (std::size_t i = 0; i < histos.size(); ++i) {
        histos.at(i)->SetLineWidth(2);
        if (i < m_styles.size()) {
            histos.at(i)->SetLineColor(m_styles.at(i).first);
            histos.at(i)->SetLineStyle(m_styles.at(i).second);
        } else {
            std::cerr << "Plotter::PlotUpperComparison: No map for the plot style. Will use some random colours.\n";
            histos.at(i)->SetLineColor(i);
            histos.at(i)->SetLineStyle(i);
        }

        if (i == 0) {
            histos.at(0)->Draw("HIST");
        } else {
            histos.at(i)->Draw("HIST same");
        }
    }

    DrawLabels(pad, 0.2 , 0.85, false);
}

void Plotter::PlotRatioComparison(const std::vector<std::unique_ptr<TH1D> >& histos,
                                  TPad* pad,
                                  const std::string& axis,
                                  const float& range,
                                  const bool& force) const {

    if (histos.size() < 2) return;

    pad->cd();

    histos.at(1)->GetXaxis()->SetLabelFont(42);
    histos.at(1)->GetXaxis()->SetLabelSize(0.15);
    histos.at(1)->GetXaxis()->SetLabelOffset(0.01);
    histos.at(1)->GetXaxis()->SetTitleFont(42);
    histos.at(1)->GetXaxis()->SetTitleSize(0.15);
    histos.at(1)->GetXaxis()->SetTitleOffset(1.2);
    histos.at(1)->GetXaxis()->SetTitle(axis.c_str());

    histos.at(1)->GetYaxis()->SetLabelFont(42);
    histos.at(1)->GetYaxis()->SetLabelSize(0.15);
    histos.at(1)->GetYaxis()->SetLabelOffset(0.03);
    histos.at(1)->GetYaxis()->SetTitleFont(42);
    histos.at(1)->GetYaxis()->SetTitleSize(0.15);
    histos.at(1)->GetYaxis()->SetTitleOffset(0.5);
    histos.at(1)->GetYaxis()->SetNdivisions(505);
    histos.at(1)->GetYaxis()->SetTitle("ratio");

    float max(-10);
    float min(100);
    for (std::size_t ihist = 1; ihist < histos.size(); ++ihist) {
        histos.at(ihist)->Divide(histos.at(0).get());
        if (histos.at(ihist)->GetMaximum() > max) {
            max = histos.at(ihist)->GetMaximum();
        }
        if (histos.at(ihist)->GetMinimum() < min) {
            min = histos.at(ihist)->GetMinimum();
        }
        if (ihist == 1) {
            histos.at(1)->Draw("HIST");
        } else {
            histos.at(ihist)->Draw("HIST same");
        } 
    }

    if (force) {
        histos.at(1)->GetYaxis()->SetRangeUser(1-range, 1.+range);
    } else {
        histos.at(1)->GetYaxis()->SetRangeUser(0.8*min, 1.2*max);
    }
}

void Plotter::FillStyleMap() {
    m_styles.push_back(std::make_pair(kBlack, 1));  
    m_styles.push_back(std::make_pair(kBlue, 1));  
    m_styles.push_back(std::make_pair(kRed, 1));  
    m_styles.push_back(std::make_pair(kGreen+2, 1));  
    m_styles.push_back(std::make_pair(kMagenta, 1));
}

float Plotter::MaxResize(const std::vector<std::unique_ptr<TH1D> >& histos, const bool& is_log) const {
    float result(-9999);

    for (const auto& ihist : histos) {
        if (result < ihist->GetMaximum()) {
            result = ihist->GetMaximum();
        }
    }

    if (is_log) return result*1e6;

    return result*1.6;
}

void Plotter::DrawLabels(TPad *pad, const float& x, const float& y, const bool& add_lumi) const{
    pad->cd();

    TLatex l1;
    l1.SetTextAlign(9);
    l1.SetTextFont(72);
    l1.SetTextSize(0.06);
    l1.SetNDC();
    l1.DrawLatex(x, y, "ATLAS");

    TLatex l2;
    l2.SetTextAlign(9);
    l2.SetTextSize(0.06);
    l2.SetTextFont(42);
    l2.SetNDC();
    l2.DrawLatex(x+0.11, y, m_atlas_label.c_str());
    
    TLatex l3;
    l3.SetTextAlign(9);
    l3.SetTextSize(0.06);
    l3.SetTextFont(42);
    l3.SetNDC();
    if (add_lumi) {
        l3.DrawLatex(x, y-0.07, ("#sqrt{s} = 13 TeV, "+m_lumi_label+" fb^{-1}").c_str());
    } else {
        l3.DrawLatex(x, y-0.07, "#sqrt{s} = 13 TeV");
    }

    std::string collection;
    /// Add collection name
    if (m_collection == "topo")  {
        collection = "Anti-kt, R=0.4, EMTopo";
    } else if (m_collection == "pflow") {
        collection = "Anti-kt, R=0.4, EMPFlow";
    } else {
        std::cout << "Plotter::DrawLabels: Collection not set, setting to 'pflow'\n";
        collection = "Anti-kt, R=0.4, EMPFlow";
    }

    l2.DrawLatex(x, y-0.15, collection.c_str());
}
    
void Plotter::DrawUpperDataMCPlot(TPad* pad,
                                  TH1D* ttbar_histo,
                                  const std::vector<std::unique_ptr<TH1D> >& background_histos,
                                  TH1D* data_histo,
                                  TGraphAsymmErrors* error) const {

    pad->cd();
    data_histo->SetMarkerStyle(20);	
    data_histo->SetMarkerSize(1.45);	
    data_histo->SetLineColor(kBlack);	
    data_histo->SetLineStyle(1);	
    
    ttbar_histo->GetYaxis()->SetLabelSize(0.05);
    ttbar_histo->GetYaxis()->SetLabelFont(42);
    ttbar_histo->GetYaxis()->SetTitleFont(42);
    ttbar_histo->GetYaxis()->SetTitleSize(0.07);
    ttbar_histo->GetYaxis()->SetTitleOffset(1.1);


    ttbar_histo->Draw("HIST");
    for (auto& ibkg : background_histos) {
        ibkg->Draw("HIST SAME");
    }

    data_histo->Draw("X0EPZ SAME");
    error->Draw("E2 SAME");

    DrawLabels(pad, 0.21, 0.88, true);
}

void Plotter::DrawRatioDataMCPlot(TPad* pad,
                                  TH1D* combined,
                                  TH1D* data,
                                  const std::string& axis,
                                  TGraphAsymmErrors* error) const {
    pad->cd();

    data->Divide(combined);
    data->SetMarkerStyle(8);
    data->SetMarkerSize(1);
    data->GetXaxis()->SetLabelFont(42);
    data->GetXaxis()->SetLabelSize(0.15);
    data->GetXaxis()->SetLabelOffset(0.01);
    data->GetXaxis()->SetTitleFont(42);
    data->GetXaxis()->SetTitleSize(0.20);
    data->GetXaxis()->SetTitleOffset(0.9);
    data->GetXaxis()->SetNdivisions(505);
    data->GetXaxis()->SetTitle(axis.c_str());
    
    data->GetYaxis()->SetRangeUser(0.7,1.3);
    data->GetYaxis()->SetLabelFont(42);
    data->GetYaxis()->SetLabelSize(0.15);
    data->GetYaxis()->SetLabelOffset(0.03);
    data->GetYaxis()->SetTitleFont(42);
    data->GetYaxis()->SetTitleSize(0.15);
    data->GetYaxis()->SetTitleOffset(0.5);
    data->GetYaxis()->SetNdivisions(505);
    data->GetYaxis()->SetTitle("data/pred.");

    data->Draw("EP SAME");
    error->Draw("E2 SAME");
}

void Plotter::SetColourMap() {
    m_colour_map["ttbar_impossible"] = kSpring-7;
    m_colour_map["SingleTop_PP8_s_chan_FS"] = kBlue-1;
    m_colour_map["SingleTop_PP8_t_chan_FS"] = kBlue+1;
    m_colour_map["SingleTop_PP8_tW_chan_FS"] = kBlue;
    m_colour_map["Wjets_Sherpa_FS"] = 92;
    m_colour_map["Zjets_Sherpa_FS"] = 95;
    m_colour_map["Diboson_Sherpa_FS"] = 5;
    m_colour_map["ttV_aMCNLO_Pythia8_FS"] = kRed;
    m_colour_map["ttH_PP8_FS"] = kRed;
    m_colour_map["Multijet"] = 619;
}

void Plotter::GetTotalUpDownUncertainty(TH1D* total_up, TH1D* total_down, const std::string& name) const {
    /// Set bin content to 0
    const int nbins = total_up->GetNbinsX();

    if (total_down->GetNbinsX() != nbins) {
        std::cerr << "Plotter::GetTotalUpDownUncertainty: Up and Down histograms have different number of bins!" << std::endl;
        exit(EXIT_FAILURE);
    }

    for (int ibin = 1; ibin <= nbins; ++ibin) {
        total_up->SetBinContent(ibin, 0);
        total_up->SetBinError(ibin, 0);
        total_down->SetBinContent(ibin, 0);
        total_down->SetBinError(ibin, 0);
    }

    /// Loop over systematics and add uncertainties
    for (const auto& isyst : m_systematics) {
        AddSingleSyst(total_up, total_down, name, isyst);
    }
}

void Plotter::AddSingleSyst(TH1D* total_up, TH1D* total_down, const std::string& name, const Systematic& systematic) const {
    std::string in_file_up = systematic.up_file;
    std::string in_file_down = systematic.down_file;
    if (in_file_down == "") in_file_down = in_file_up;


    std::string up_name = systematic.up_histo + "/" + name;
    std::string down_name = systematic.down_histo + "/" + name;
    if (systematic.type == SYSTEMATICTYPE::TWOSIDED && down_name == "") {
        std::cerr << "Plotter::AddSingleSyst: You need to provide both up and down name for TWOSIDED systematic" << std::endl;
        exit(EXIT_FAILURE);
    }
    if (down_name == "") down_name == up_name;

    std::string reference_file = systematic.reference_file;
    if (reference_file == "") reference_file = in_file_up;

    std::string reference_histo = "nominal/"+name;
    if (systematic.reference_histo != "") reference_histo = systematic.reference_histo + "/" + name;
    
    /// Read the histograms
    std::unique_ptr<TH1D> histo_nominal = GetHistoFromAll(reference_file, reference_histo);
    std::unique_ptr<TH1D> histo_up(nullptr);
    std::unique_ptr<TH1D> histo_down(nullptr);
    
    if (!histo_nominal) {
        std::cerr << "Plotter::AddSingleSyst: Cannot read the necessary histograms for " << name << std::endl;
        exit(EXIT_FAILURE);
    }


    if (systematic.type == SYSTEMATICTYPE::TWOSIDED) {
        histo_up   = GetHistoFromAll(in_file_up, up_name);
        histo_down = GetHistoFromAll(in_file_down, down_name);
    } else if (systematic.type == SYSTEMATICTYPE::ONESIDED) {
        histo_up   = GetHistoFromAll(in_file_up, up_name);
        /// Symmetrise it
        std::unique_ptr<TH1D> tmp(static_cast<TH1D*>(histo_up->Clone()));
        tmp->Add(histo_nominal.get(), -1);
        histo_down = std::unique_ptr<TH1D>(static_cast<TH1D*>(histo_nominal->Clone()));
        histo_down->Add(tmp.get(), -1);
    } else if (systematic.type == SYSTEMATICTYPE::NORMALISATION) {
        histo_up   = std::unique_ptr<TH1D>(static_cast<TH1D*>(histo_nominal->Clone()));
        histo_down = std::unique_ptr<TH1D>(static_cast<TH1D*>(histo_nominal->Clone()));
        histo_up  ->Scale(1.+systematic.norm_up);
        histo_down->Scale(1.-systematic.norm_down);
    } else if (systematic.type == SYSTEMATICTYPE::MCSTAT) {
        histo_up   = std::unique_ptr<TH1D>(static_cast<TH1D*>(histo_nominal->Clone()));
        histo_down = std::unique_ptr<TH1D>(static_cast<TH1D*>(histo_nominal->Clone()));

        for (int ibin = 1; ibin <= histo_nominal->GetNbinsX(); ++ibin) {
            const double nominal = histo_nominal->GetBinContent(ibin);
            const double unc = histo_nominal->GetBinError(ibin);
            histo_up  ->SetBinContent(ibin, nominal + unc);
            histo_down->SetBinContent(ibin, nominal - unc);
        }
    }
    
    if (!histo_up || !histo_down) {
        std::cerr << "Plotter::AddSingleSyst: Cannot read the necessary up/down histograms for " << name  << std::endl;
        exit(EXIT_FAILURE);
    }

    if (total_up->GetNbinsX() != histo_down->GetNbinsX()) {
        std::cerr << "Plotter::AddSingleSyst: Total up and up syst histograms have different binning" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (histo_up->GetNbinsX() != histo_down->GetNbinsX()) {
        std::cerr << "Plotter::AddSingleSyst: Up and down syst histograms have different binning" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (histo_up->GetNbinsX() != histo_nominal->GetNbinsX()) {
        std::cerr << "Plotter::AddSingleSyst: Up syst and nominal histograms have different binning" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (m_syst_shape_only) {
        if (std::fabs(histo_up->Integral()) > 1e-6) histo_up->Scale(histo_nominal->Integral()/histo_up->Integral());
        if (std::fabs(histo_down->Integral()) > 1e-6) histo_down->Scale(histo_nominal->Integral()/histo_down->Integral());
    }

    AddHistosInSquares(total_up, total_down, histo_up.get(), histo_down.get(), histo_nominal.get());
}

std::unique_ptr<TH1D> Plotter::GetHistoFromAll(const std::string& file_name, const std::string& histo_name) const {
    /// search signal files
    auto itr_signal = std::find(m_ttbar_names.begin(), m_ttbar_names.end(), file_name);
    if (itr_signal != m_ttbar_names.end()) {
        const std::size_t pos_signal = std::distance(m_ttbar_names.begin(), itr_signal);
        return std::unique_ptr<TH1D>(static_cast<TH1D*>(m_ttbar_files.at(pos_signal)->Get(histo_name.c_str())));
    }

    /// Try bkg file
    auto itr_background = std::find(m_background_names.begin(), m_background_names.end(), file_name);
    if (itr_background != m_background_names.end()) {
        const std::size_t pos_bkg = std::distance(m_background_names.begin(), itr_background);
        return std::unique_ptr<TH1D>(static_cast<TH1D*>(m_background_files.at(pos_bkg)->Get(histo_name.c_str())));
    }

    /// Has to be special
    auto itr_special = std::find(m_special_names.begin(), m_special_names.end(), file_name);
    if (itr_special == m_special_names.end()) {
        std::cerr << "Plotter::GetHistoFromAll: Cannot find file: " << file_name << std::endl;
        exit(EXIT_FAILURE);
    }
    
    const std::size_t pos_special = std::distance(m_special_names.begin(), itr_special);
    
    return std::unique_ptr<TH1D>(static_cast<TH1D*>(m_special_files.at(pos_special)->Get(histo_name.c_str())));
}

void Plotter::AddHistosInSquares(TH1D* total_up, TH1D* total_down, TH1D* histo_up, TH1D* histo_down, TH1D* histo_nominal) const {

    /// We know the histograms have the same binning
    const int nbins = total_up->GetNbinsX();
    for (int ibin = 1; ibin <= nbins; ++ibin) {
        double up(0);
        double down(0);
        if (histo_up->GetBinContent(ibin) > histo_nominal->GetBinContent(ibin)) {
            up += histo_up->GetBinContent(ibin) - histo_nominal->GetBinContent(ibin);
        } else {
            down += histo_nominal->GetBinContent(ibin) - histo_up->GetBinContent(ibin);
        }
        if (histo_down->GetBinContent(ibin) > histo_nominal->GetBinContent(ibin)) {
            up += std::sqrt((up * up) + ((histo_down->GetBinContent(ibin) - histo_nominal->GetBinContent(ibin)) * (histo_down->GetBinContent(ibin) - histo_nominal->GetBinContent(ibin))));
        } else {
            down += std::sqrt((down * down) + ((histo_nominal->GetBinContent(ibin) - histo_down->GetBinContent(ibin)) * (histo_nominal->GetBinContent(ibin) - histo_down->GetBinContent(ibin))));
        }

        /// now add it to the total hist
        const double current_up   = total_up->GetBinContent(ibin);
        const double current_down = total_down->GetBinContent(ibin);

        total_up  ->SetBinContent(ibin,  std::hypot(current_up, up));
        total_down->SetBinContent(ibin, -std::hypot(current_down, down));
    }
}

void Plotter::TransformErrorHistoToTGraph(TGraphAsymmErrors* error, TH1D* up, TH1D* down) const {
    const int nbins = error->GetN();

    for (int ibin = 0; ibin < nbins; ++ibin) {
        error->SetPointEYhigh(ibin, up->GetBinContent(ibin+1));
        error->SetPointEYlow (ibin, -down->GetBinContent(ibin+1));
    }
}

void Plotter::SetLabelMap() {
    m_label_map["ttbar_PP8_FS"] = "t#bar{t} PP8";
    m_label_map["ttbar_impossible"] = "t#bar{t} not matched";
    m_label_map["ttbar_PP8_AFII"] = "t#bar{t} PP8 AFII";
    m_label_map["ttbar_PP8_dilep_FS"] = "t#bar{t} PP8";
    m_label_map["ttbar_PP8_dilep_AFII"] = "t#bar{t} PP8 AFII";
    m_label_map["Wjets_Sherpa_FS"] = "W+jets";
    m_label_map["Zjets_Sherpa_FS"] = "Z+jets";
    m_label_map["Diboson_Sherpa_FS"] = "Diboson";
    m_label_map["ttV_aMCNLO_Pythia8_FS"] = "t#bar{t}+V/H";
    m_label_map["ttH_PP8_FS"] = "t#bar{t}+V/H";
    m_label_map["SingleTop_PP8_s_chan_FS"] = "SingleTop s-chan";
    m_label_map["SingleTop_PP8_t_chan_FS"] = "SingleTop t-chan";
    m_label_map["SingleTop_PP8_tW_chan_FS"] = "SingleTop tW-chan";
    m_label_map["SingleTop_PP8_tW_chan_dilep_FS"] = "SingleTop tW-chan";
}
