#!/usr/bin/env python2

from sys import argv, exit
from ROOT import *

import re


# ======================= define DSIDs here: ============================
ttbar_dsids = [410470]
ttbar_PowHer7_dsids = [410557, 410558]
ttbar_sherpa_dsids = [410250, 410251, 410252]
ttbar_syst_dsids = [i for i in range(410001, 410005)] + [410500, 410159]
ttbar_shower_decor_dsids = [411296, 411297]
single_top_s_dsids = [410644, 410645]
single_top_t_dsids = [410658, 410659]
single_top_tW_dsids = [410646, 410647]
single_top_tW_dilep_dsids = [410648, 410649]
wjets_dsids = [i for i in range(364156, 364198)] + [i for i in range(363436, 363484)]
zjets_dsids = [i for i in range(364100, 364142)]+ [i for i in range(363361, 363412)]
diboson_dsids = [i for i in range(361063, 361095)] + [361096, 363356, 363358, 363359, 363489, 364250, 364253, 364254, 364255, 363360]
ttv_dsids = [i for i in range(410155, 410158)] + [i for i in range(410218, 410221)] + [410081]
tth_dsids = [i for i in range(343365, 343368)] + [i for i in range(345672, 345675)] + [i for i in range(345873, 345876)] + [i for i in range(346343, 346346)]
ttbar_aMCNLO = [410464,410465]
ttbar_mass_171 = [411053, 411045]
ttbar_mass_172 = [411054, 411046]
ttbar_mass_173 = [411057, 411049]
ttbar_mass_174 = [411058, 411050]
ttbar_aMCNLO_Herwig = [412116,412117]
ttbar_Herwig_7_1_3 = [411233,411234]
single_top_tW_DS_dsids = [410654, 410655]
single_top_tW_DS_dilep_dsids = [410656, 410657]
single_top_s_PowHer7_dsids = [411034 ,411035]
single_top_t_PowHer7_dsids = [411032, 411033]
single_top_tW_PowHer7_dsids = [411036, 411037]
single_top_tW_PowHer7_dilep_dsids = [411038, 411039]
single_top_s_aMCNLO_dsids = [412005]
single_top_t_aMCNLO_dsids = [412004]
single_top_tW_aMCNLO_dsids = [412002]
single_top_tW_aMCNLO_dilep_dsids = [412003]
ttbar_Hdamp_dsids = [410480, 410482]

# =======================================================================

def main():
    if len(argv) != 3:
        print "Usage: {} <input list of files> <output list of files with sum of weights> ".format(argv[0])
        exit(1)

    if argv[1] == argv[2]:
        print "ERROR: Input list and output list cannot be the same!"
        exit(1)

    fin = open(argv[1], 'r')
    fout = open(argv[2], 'w')
    processFile(fin, fout)
    fout.close()
    fin.close()

    print "Done."

def processFile(fin, fout):
    files = fin.readlines() # NOTE, each line has a '\n' at the end
    print "Writing file list with sumOfWeights values..."

    # for each output dataset file, write full path and sum of weights
    # in the output file list
    for f in [line.strip('\n') for line in files]:

        # Get DSID
        rootfile = TFile(f, 'read')
        tree = rootfile.Get('nominal')

        af2 = 'FS'
        DSID = -1
        runNumber = -1
        campaign = 'mc16a'
        if tree:
            if tree.GetEntries() == 0:
                print "Skipping empty sample: {}".format(f)
                continue
            tree.GetEntry(0)
            DSID = tree.mcChannelNumber
            if DSID > 10000:
                afii = tree.is_AFII
                if afii == '\x01':
                    af2='AFII'
            if DSID > 1000:
                runNumber = tree.randomRunNumber
            else:
                runNumber = tree.runNumber
            if (runNumber > 276261 and runNumber < 311482):
                campaign = 'mc16a'
            elif (runNumber > 325712 and runNumber < 348836):
                campaign = 'mc16d'
            elif (runNumber > 348835):
                campaign = 'mc16e'

            if DSID > 0:
                current_key = str(DSID) + af2 + campaign
            else:
                current_key = str(DSID)

        else:
            print "Skipping sample without nominal(_Loose) tree: {}".format(f)
            continue

        fout.write(f + ' ')
        fout.write(getSampleType(DSID, runNumber, af2) + ' ')
        fout.write(af2 + ' ' )
        fout.write(str(DSID) + ' ' )
        fout.write(campaign + '\n' )

def getSampleType(DSID, runNumber, af2):
    if DSID == 0:
        if runNumber >= 276262 and runNumber <= 284484:
            return 'Data'
        elif runNumber >= 297730:
            return 'Data'
        else:
            return 'unknown'
    elif DSID in ttbar_dsids:
        return 'ttbar_PP8_' + af2
    elif DSID in ttbar_PowHer7_dsids:
        return 'ttbar_PH7_0_4_' + af2
    elif DSID in ttbar_sherpa_dsids:
        return 'ttbar_Sherpa_' + af2
    elif DSID in single_top_s_dsids:
        return 'SingleTop_PP8_s_chan_' + af2
    elif DSID in single_top_t_dsids:
        return 'SingleTop_PP8_t_chan_' + af2
    elif DSID in single_top_tW_dsids:
        return 'SingleTop_PP8_tW_chan_' + af2
    elif DSID in single_top_tW_dilep_dsids:
        return 'SingleTop_PP8_tW_chan_dilep_' + af2
    elif DSID in single_top_tW_DS_dsids:
        return 'SingleTop_DS_PP8_tW_chan_' + af2
    elif DSID in single_top_tW_DS_dilep_dsids:
        return 'SingleTop_DS_PP8_tW_chan_dilep_' + af2
    elif DSID in single_top_s_PowHer7_dsids:
        return 'SingleTop_PH7_0_4_s_chan_' + af2
    elif DSID in single_top_t_PowHer7_dsids:
        return 'SingleTop_PH7_0_4_t_chan_' + af2
    elif DSID in single_top_tW_PowHer7_dsids:
        return 'SingleTop_PH7_0_4_tW_chan_' + af2
    elif DSID in single_top_tW_PowHer7_dilep_dsids:
        return 'SingleTop_PH7_0_4_tW_chan_dilep_' + af2
    elif DSID in single_top_s_aMCNLO_dsids:
        return 'SingleTop_aMCNLO_Pythia8_s_chan_' + af2
    elif DSID in single_top_t_aMCNLO_dsids:
        return 'SingleTop_aMCNLO_Pythia8_t_chan_' + af2
    elif DSID in single_top_tW_aMCNLO_dsids:
        return 'SingleTop_aMCNLO_Pythia8_tW_chan_' + af2
    elif DSID in single_top_tW_aMCNLO_dilep_dsids:
        return 'SingleTop_aMCNLO_Pythia8_tW_chan_dilep_' + af2
    elif DSID in wjets_dsids:
        return 'Wjets_Sherpa_' + af2
    elif DSID in zjets_dsids:
        return 'Zjets_Sherpa_' + af2
    elif DSID in diboson_dsids:
        return 'Diboson_Sherpa_' + af2
    elif DSID in ttv_dsids:
        return 'ttV_aMCNLO_Pythia8_' + af2
    elif DSID in tth_dsids:
        return 'ttH_PP8_' + af2
    elif DSID in ttbar_aMCNLO:
        return 'ttbar_aMCNLO_Pythia8_' + af2
    elif DSID in ttbar_aMCNLO_Herwig:
        return 'ttbar_aMCNLO_Herwig7_1_3_' + af2
    elif DSID in ttbar_Herwig_7_1_3:
        return 'ttbar_PH7_1_3_' + af2
    elif DSID in ttbar_mass_172:
        return 'ttbar_PP8_mass_172_' + af2
    elif DSID in ttbar_mass_173:
        return 'ttbar_PP8_mass_173_' + af2
    elif DSID in ttbar_Hdamp_dsids:
        return 'ttbar_PP8_hdamp_' + af2
    elif DSID in ttbar_shower_decor_dsids:
        return 'ttbar_PP8_shower_decor_' + af2
    else:
        return 'unknown'

# run the script (if executed, and *not* imported as a module)
if __name__ == '__main__':
    main()
